<?php
/**
 * Created by PhpStorm.
 * User: Developer
 * Date: 02-Feb-19
 * Time: 04:47 PM
 */

Route::group([
    // 'middleware' => ['auth:admin','role:admin|user'],
], function () {
    Route::group(['namespace' => 'DemoCrud','middleware' => 'check_user_is_active'], function () {

        /*  For DataTables */
        #Route::any('/demo-crud-lists', 'DemoCrudTableController')->name('demo-crud.lists');

        Route::resource('/demo-crud', 'DemoCrudController')->except(['show','destroy']);
        Route::any('/demo-crud/list', 'DemoCrudController@index')->name('demo-crud.list'); //AJ Route
        Route::any('/demo-crud/add', 'DemoCrudController@index')->name('demo-crud.add'); //AJ Route
        Route::any('/demo-crud/update/{id?}', 'DemoCrudController@index')->name('demo-crud.update');

        Route::any('/demo-crud/lists', 'DemoCrudController@getList')->name('demo-crud.get-list'); //Fetching List Using AJ Call
        Route::post('/demo-crud/record-add', 'DemoCrudController@store')->name('demo-crud.record-add');
        Route::post('/demo-crud/record-update/{id?}', 'DemoCrudController@update')->name('demo-crud.record-update');
        Route::get('/demo-crud/get-detail/{id?}', 'DemoCrudController@getDetailById')->name('demo-crud.get-detail');

        Route::get('/demo-crud-delete/{frontUser}','DemoCrudController@destroy')->name('demo-crud.destroy');

        Route::any('demo-crud/change-status', 'DemoCrudController@changeStatus')->name('demo-crud.change-status');
        Route::any('demo-crud/destroy','DemoCrudController@destroy')->name('demo-crud.destroy');
        Route::any('demo-crud/bulk-action','DemoCrudController@bulkAction')->name('demo-crud.bulk-action');
      #  Route::any('ngBreadCrumbTemplate', 'DemoCrudController@getB')->name('demo-crud.get-b');

    });
});