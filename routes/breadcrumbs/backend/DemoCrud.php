<?php
/**
 * Created by Backend Brains.
 * User: Developer
 * Date: 20-Jan-19
 * Time: 4:20 PM
 */

Breadcrumbs::for('admin.demo-crud.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Manage CRUD', route('admin.demo-crud.index'));
});

Breadcrumbs::for('admin.demo-crud.create', function ($trail) {
    $trail->parent('admin.demo-crud.index');
    $trail->push('Create Record', route('admin.demo-crud.create'));
});

Breadcrumbs::for('admin.demo-crud.edit', function ($trail,$id) {
    $trail->parent('admin.demo-crud.index');
    $trail->push('Edit Record', route('admin.demo-crud.edit',$id));
});

