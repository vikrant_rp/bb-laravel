<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Test extends Model
{
    use HasFactory;

    protected $table = 'tests';
    public $timestamps = true;

    protected $casts = [
        'price' => 'float'
    ];

    protected $fillable = [
        'name',
        'description',
        'price',
        'created_at'
    ];
    var $columnOrder = array(null,'name', 'description', 'status'); //set column field database for datatable orderable
    var $columnSearch = array('name','description', 'price', 'status'); //set column field database for datatable searchable
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * For Datatable process start
     * This function is used for Datatable list
     * @return object
     */
    public function getList($filterData='')
    {
        #DB::enableQueryLog(); // Enable query log
        $dataBase = $this->getData($filterData);
        if ($_POST['length'] != -1)
            $dataBase->skip($_POST['start'])->limit($_POST['length']);
            #$dataBase->limit($_POST['length'], $_POST['start']);#$this->db->limit($_POST['length'], $_POST['start']);
        $responseData  = $dataBase->get();
        #dd(DB::getQueryLog()); // Show results of log
        return $responseData ;//$query->all();
    }

    private function getData($filterData='')
    {
        $dataBase = $this->fetchData($filterData);
        $columnSearch = $this->columnSearch;
        $searchKeyWord = $_POST['search']['value'];
        if(!empty($searchKeyWord)) // if datatable send POST for search
        {
            #$dataBase->where('name', 'like', '%' . $searchKeyWord . '%');
            $dataBase->where(function($q) use ($columnSearch,$searchKeyWord) {
                foreach($columnSearch as $key => $colName) {
                    if ($key == 0) {
                        $q->where($colName, 'like', '%' . DB::raw($searchKeyWord) . '%');
                    } else {
                        $q->orWhere($colName, 'like', '%' . DB::raw($searchKeyWord) . '%');
                    }
                }
            });
        }
        #if (isset($_POST['order'])) // here order processing
        if (isset($_POST['order'])) // here order processing
        {
            $dataBase->orderBy($this->columnOrder[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            //$this->db->order_by($this->columnOrder[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            #$this->db->order_by(key($order), $order[key($order)]);
            $dataBase->orderBy(key($order), $order[key($order)]);
        }
        return $dataBase;
    }

    private function fetchData($filterData=''){
        $dataBase = DB::table($this->table)->select('*');
        $dataBase->where('status', '<>', "0");
        if(!empty($filterData)){
            if(isset($filterData['filterStatus'])){
                $dataBase->where('status', '=', $filterData['filterStatus']);
            }
        }
        return $dataBase;
    }

    public function countFiltered($filterData='')
    {
        $dataBase = $this->getData($filterData);
        $responseData = $dataBase->get()->count();
        return $responseData;
    }

    public function countAll($filterData='')
    {
        $dataBase = $this->fetchData();
        $responseData = $dataBase->get()->count();
        return $responseData;
    }

    /** Datatable Process End **/
    public function getRecordById($id){
        $dataBase = DB::table($this->table)->select('*');
        $dataBase->where('id', '=', $id);
        if(!empty($filterData)){
            if(isset($filterData['filterStatus'])){
                #$dataBase->where('status', '=', $filterData['filterStatus']);
            }
        }
        $responseData  = $dataBase->get()->first();
        return $responseData;
    }


    public function addRecord($crudData)
    {
        $returnId = DB::table($this->table)->insertGetId($crudData);
        return $returnId;
    }
    public function updateRecord($crudData, $where)
    {
        $affectedRow = DB::table($this->table)->where($where)->update($crudData);
        #$affectedRow = DB::table($this->table)->where($where)->limit(1)->update($crudData);
        return $affectedRow;
    }

}