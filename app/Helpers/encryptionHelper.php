<?php
/**
 * Created by PhpStorm.
 * User: Backend Brains
 * Date: 2021
 * Use: Encryption/Decryption String
 */

use App\Libs\EncryptionData;


/**
 * $string = Plain Text
 * @return Encrypted Values
 */
if (!function_exists('_encrypt')) {

    function _encrypt($string)
    {
        #$objEncryption = new \App\Libs\EncryptionData();
        $objEncryption = new EncryptionData();
        if(!empty($string))
        {
            $string = $objEncryption->encode($string);
        }
        return $string ;
    }
}


/**
 * $string = Encrypted Text
 * @return Decrypted Values
 */
if (!function_exists('_decrypt')) {

    function _decrypt($string)
    {
        #$objEncryption = new \App\Libs\EncryptionData();
        $objEncryption = new EncryptionData();
        if(!empty($string))
        {
            $string = $objEncryption->decode($string);
        }
        return $string ;
    }
}