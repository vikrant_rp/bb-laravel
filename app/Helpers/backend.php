<?php
/**
 * Created by PhpStorm.
 * User: Backend Brains
 * Date: 2021
 * Use: Manage Admin Panel
 */

use Carbon\Carbon;
use Illuminate\Support\Str;



/**
 * $module = database table name
 * $status = integer key of status
 * @return Admin Panel STATUS column HTML View
 */
if (!function_exists('_column_status')) {

    function _column_status($status,$module=[])
    {
        $htmlStatus = '';
        if(!empty($module))
        {
            $moduleArray = @config('backend.status.'.$module)[$status];
            #dd($moduleArray );
            if(!empty($moduleArray )){
                $htmlStatus = '<a href="javascript:void(0)" class="status" ><small class="badge badge-'.$moduleArray['color'].'" >'.$moduleArray['label'].'</small></a>';
            }
            //config('backend.status')
        }
        return $htmlStatus;
    }
}
/**
 * $id = Primary Id
 * @return Admin Panel Edit Button HTML View
 */
if (!function_exists('_button_edit')) {

    function _button_edit($url='')
    {
        #$htmlStatus = '<button type="button" class="btn btn-'.config('backend.colors.warning').' btn-sm" id="editRecord" data-id="'.$id.'">Edit</button>';
        $htmlStatus = '<a href="'.$url.'" class="btn btn-'.config('backend.colors.warning').' btn-sm" id="editRecord">Edit </a>';

        return $htmlStatus;
    }
}

if (!function_exists('_required_asterisk')) {

    function _required_asterisk($msg= 0)
    {
        if($msg){
            $htmlView = '<span class="pull-right text-danger">* required fields</span>';
        }else{
            $htmlView = '<span class="required text-danger">*</span>';
        }
        return $htmlView;


    }
}
/**
 * $module = database table name
 * $exclude = array which is exclude from status array
 * @return Status Array with Data base Status Key and Value/Display Name
*/
if (!function_exists('_status_array')) {

    function _status_array($module,$exclude=[])
    {
        $status = config('backend.status.'.$module);
        $newStatus = [];
        if($status){
            foreach($status  as $k => $row){
                if(!empty($exclude)){
                    if(!in_array($k,$exclude)){
                        $newStatus[$k] = $row['label'];
                    }
                }else{
                    $newStatus[$k] = $row['label'];
                }
            }
        }
        return $newStatus ;
    }
}
