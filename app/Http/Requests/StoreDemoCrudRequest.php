<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDemoCrudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'max:255|nullable',
            'price' => 'required|digits:10',
            'status' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required',
            #'email.unique' => 'Email already register',
            'description.required' => 'Description number is required',
            'status.numeric' => 'Enter 10 Digit mobile number',
            'mobile_number.length' => 'Enter 10 Digit mobile number',
            'mobile_number.unique' => 'Mobile number already registered',
            'user_type.required' => 'Select user type',
            'password.required' => 'Password is required',
            'password.min' => 'Password should have atleast 6 character',
        ];
    }
}
