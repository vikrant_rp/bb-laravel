<?php

namespace App\Http\Controllers\Backend\DemoCrud;
#ini_set('display_errors', 1);
#ini_set('display_startup_errors', 1);
#error_reporting(E_ALL);

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDemoCrudRequest;
use App\Repositories\Backend\DemoCrud\DemoCrudRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use App\Repositories\Backend\Role\RoleRepository;
use Illuminate\Session\Store;
use App\Models\Backend\GlobalModules\Package\Package;
use App\Models\Backend\Test;
use Illuminate\Validation\Rule;

class DemoCrudController extends Controller
{
    /**
     * @var currentClassRepository
     */
    protected $currentClassRepo;


    /**
     * currentClassRepository constructor.
     * @param currentClassRepository $frontUser
     */
    public function __construct(Test $test)
    {
        $this->middleware('auth:admin');
        #$this->currentClassRepo = $currentClassRepo;
        $this->objTest = $test;
        $this->pageData = new \stdClass();
        $this->pageData->pageTitle = 'DEMO CRUD Title';
        $this->pageData->mainTitle = 'DEMO CRUD';
        $this->pageData->subTitle = 'Sub Title';
        $this->status = TRUE;
        $this->statusCode = Response::HTTP_OK;
        $this->message = '';
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        try {
            #toastr()->error('Data has been updated successfully!');
            #$this->pageData->mainTitle = 'DEMO CRUD List';
            $this->pageData->subTitle = 'Demo CRUD List';
            $this->pageData->listTitle = 'Demo CRUD List';
            $this->pageData->addTitle = 'Add New Record';
            $this->pageData->updateTitle = 'Update record';
            $pageData = $this->pageData;
            $packageLists = null;//Package::all()->pluck('name', 'id')->toArray();
            return view(BACKEND_VIEW.'.demo-crud.index',compact('pageData','packageLists'));
            #return view('backend.front-user.index',compact('packageLists'));
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
        }
    }
    /**
     *  Datatable List
    **/
    public function getList(Request $request)
    {
        try {
            $filterData = @$request->filters;
            $list = $this->objTest->getList($filterData);
            $data = array();
            $no = $request->start;//$_POST['start'];
            $list_cms_status = [];//$this->config->item('tbl_column_status_array1')['status'];
            foreach ($list as $key => $record) {
                $no++;
                $idEncrypted = _encrypt($record->id);

                $urlEdit =  route('admin.demo-crud.update',['id'=>$idEncrypted]) ;//route('demo-crud/update',['id'=>21]);//'update/'.$idEncrypted;
                $row = array();
                //$row[] = $idEncrypted;//'<input type="checkbox" class="child" name="id[]" value="'.$idEncrypted.'">';
                /*$row[] = '<div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="customCheckbox_'.$no.'" name="id[]" value="'.$idEncrypted.'">
                          <label for="customCheckbox_'.$no.'" class="custom-control-label"></label>
                        </div>';*/
                $row[] = $no;
                $row[] = ucfirst(trim($record->name));
                $row[] = (trim($record->description));
                #$row[] = (trim($record->status));
                $row[] = _column_status(trim($record->status),'tests'); //tests = table name
                /*$row[] = $record->status == 1 ? '<a href="javascript:void(0)" class="status" ><small class="badge badge-success" >Active</small></a>'
                    : '<a href="javascript:void(0)" class="status" ><small class="badge badge-danger">In-active</small></a>';*/
                $shop_btn = '';
                $row[] = _button_edit($urlEdit);//edit_button($edit_url);
                $data[] = $row;
            }
            $output = array(
                "draw" => $request->draw,//$_POST['draw'],
                "recordsTotal" => $this->objTest->countAll(),
                "recordsFiltered" => $this->objTest->countFiltered($filterData),
                "data" => $data,
            );
            //output to json format
            echo json_encode($output);exit;
        } catch (\Exception $ex) {
            $output = array(
                "draw" => $request->draw,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => [],
            );
            Log::error($ex->getMessage());
            #echo json_encode($ex->getMessage());exit;
            echo json_encode($output);exit;
        }
    }

    /**
     * @param StoreFrontUserRequest $request
     * @return mixed
     */
    //
    public function store(Request $request) //StoreDemoCrudRequest $request
    {
        try {
            $rule['name'] = ['required']; //|unique:name
            $rule['description'] = ['required','max:255'];
            $rule['price'] = ['required','numeric'];
            $rule['status'] = ['required',Rule::in(array_keys(_status_array('tests')))];
            $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rule);
            if ($validator->fails()) {
                $errors = array();
                $fieldsWithErrorMessagesArray = $validator->messages()->get('*');
                if($fieldsWithErrorMessagesArray ){
                    foreach($fieldsWithErrorMessagesArray  as $key =>$row){
                        $errors[$key] = $row[0];
                    }
                }
                $errors = implode("<br>", $validator->errors()->all());
                $this->status = false;
                $this->statusCode = Response::HTTP_BAD_REQUEST;
                $this->message = $errors;
                $responseData = array('status'=>$this->status,'message'=>$this->message,'statusCode'=>$this->statusCode,'data'=>array());
                return response()->json($responseData,$this->statusCode);
            }
            $crudData = array(
                'name'=> !empty($request->name) ? $request->name : null,
                'description'=> !empty($request->description) ? $request->description : null,
                'price'=> !empty($request->price) ? $request->price : null,
                'status'=> !empty($request->status) ? $request->status : null,
            );

            $affectedRow = $this->objTest->addRecord($crudData);
            if (!$affectedRow) {
                $this->statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
                $this->status = FALSE;
                $this->message = __('backend_messages.global_record_added_failed');
            } else {
                $this->message = __('backend_messages.global_record_added_success');
            }
            $responseData = array('status'=>$this->status,'message'=>$this->message,'statusCode'=>$this->statusCode,'data'=>array());
            return response()->json($responseData,$this->statusCode);
        } catch (\Exception $ex) {
            //Log::channel('backend')->info(['Module'=> 'doCheckout Data Send ',  'request' => $request->all()]);
            Log::error($ex->getMessage());
        }
    }
    public function update(Request $request,$id='') //StoreDemoCrudRequest $request
    {
        try {
            dd($request);
            $rule['name'] = ['required']; //|unique:name
            $rule['description'] = ['required','max:255'];
            $rule['price'] = ['required','numeric'];
            $rule['status'] = ['required',Rule::in(array_keys(_status_array('tests')))];
            $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rule);
            if ($validator->fails()) {
                $errors = array();
                $fieldsWithErrorMessagesArray = $validator->messages()->get('*');
                if($fieldsWithErrorMessagesArray ){
                    foreach($fieldsWithErrorMessagesArray  as $key =>$row){
                        $errors[$key] = $row[0];
                    }
                }
                $errors = implode("<br>", $validator->errors()->all());
                $this->status = false;
                $this->statusCode = Response::HTTP_BAD_REQUEST;
                $this->message = $errors;
                $responseData = array('status'=>$this->status,'message'=>$this->message,'statusCode'=>$this->statusCode,'data'=>array());
                return response()->json($responseData,$this->statusCode);
            }
            $crudData = array(
                'name'=> !empty($request->name) ? $request->name : null,
                'description'=> !empty($request->description) ? $request->description : null,
                'price'=> !empty($request->price) ? $request->price : null,
                'status'=> !empty($request->status) ? $request->status : null,
            );

            $affectedRow = $this->objTest->addRecord($crudData);
            if (!$affectedRow) {
                $this->statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
                $this->status = FALSE;
                $this->message = __('backend_messages.global_record_added_failed');
            } else {
                $this->message = __('backend_messages.global_record_added_success');
            }
            $responseData = array('status'=>$this->status,'message'=>$this->message,'statusCode'=>$this->statusCode,'data'=>array());
            return response()->json($responseData,$this->statusCode);
        } catch (\Exception $ex) {
            //Log::channel('backend')->info(['Module'=> 'doCheckout Data Send ',  'request' => $request->all()]);
            Log::error($ex->getMessage());
        }
    }


    public function getDetailById($id='')
    {
        try {
            $id = !empty($id ) ? _decrypt($id)  : 0;
            if(!filter_var($id,FILTER_VALIDATE_INT)){
                $id  = 0;
            }
            $responseDetail = $this->objTest->getRecordById($id);
            if (!$responseDetail) {
                $this->statusCode = Response::HTTP_OK;
                $this->status = FALSE;
                $this->message = __('backend_messages.global_record_not_found');
            } else {
                $responseDetail->id= _encrypt($responseDetail->id);
            }
            $responseData = array('status'=>$this->status,'message'=>$this->message,'statusCode'=>$this->statusCode,'data'=>$responseDetail,'id'=>$id);
            return response()->json($responseData,$this->statusCode);
        } catch (\Exception $ex) {
            //Log::channel('backend')->info(['Module'=> 'doCheckout Data Send ',  'request' => $request->all()]);
            Log::error($ex->getMessage());
        }
    }

    /**
     * @param User $frontUser
     * @return array
     * @throws \Exception
     */
    public function bulkAction(Request $request)
    {
        try {
            $actionType = $request->action_type;

            $frontUser = User::query()->whereIn('id', request('data'));

            switch ($actionType) {
                case '0':
                    $frontUser->update(['status' => 0]);
                    return response()->json(['success' => true, 'message' => 'In-Active record successfully']);
                    break;
                case '1':
                    $frontUser->update(['status' => 1]);
                    return response()->json(['success' => true, 'message' => 'Active record successfully']);
                    break;
                case '3':
                    $frontUser->delete();
                    return response()->json(['success' => true, 'message' => 'Delete record successfully']);
                    break;
                default:
                    return response()->json(['error' => true, 'message' => 'No action']);
                    break;
            }
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
        }
    }

    public function changeStatus(Request $request)
    {
        try {
            $Id = $request->id;
            $user = User::withoutGlobalScope('status')->findOrFail($Id);
            $user->status = $user->status == 1 ? 0 : 1;
            $user->save();

            return \response()->json(['success' => true, 'message' => 'Change status successfully']);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return \response()->json(['error' => true, 'message' => $ex->getMessage()]);
        }
    }



    /* OLD code Below */
    public function create()
    {
        try {
            $packageLists = null;//Package::all()->pluck('name', 'id')->toArray();
            return view('backend.front-user.create', compact('packageLists'));
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
        }
    }



    /**
     * @param FrontUser $frontUser
     * @return mixed
     */
    public function edit(User $frontUser)
    {
        try {
            $packageLists = Package::all()->pluck('name', 'id')->toArray();
            return view('backend.front-user.edit', compact('packageLists'))
                ->withFrontUser($frontUser);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
        }
    }

    /**
     * @param UpdateFrontUserRequest $request
     * @param User $frontUser
     * @return mixed
     */
    public function update1(Request $request, User $frontUser)
    {
        try {
            $this->currentClassRepo->update($frontUser, $request->only(
                'name',
                'email',
                'alternate_email',
                'mobile_number',
                'alternate_mobile_number',
                'user_type',
                'package_id',
                'description',
                'password',
                'status',
                'icon'
            ));

            toastr()->success('Data has been updated successfully!');

            return redirect()->route('admin.front-user.index');
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
        }
    }

    /**
     * @param User $frontUser
     * @return mixed
     */
    public function destroy(Request $request)
    {
        try {
            $user = User::findOrFail($request->id);
            if ($user->delete()) {
                return response()->json(['success' => true, 'message' => 'Item deleted successfully']);
            }
            return response()->json(['error' => true, 'message' => 'Internal server error']);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->json(['error' => true, 'message' => $ex->getMessage()]);
        }
    }


    public function getJson(Request $request)
    {
        try {

            $userList = $this->currentClassRepo->query()->where('name', 'like', '%' . $request->search . '%')->get();

            $resp = [];
            foreach ($userList as $key => $val) {
                $resp[] = [
                    'id' => $val->id,
                    'text' => $val->name
                ];
            }
            return response()->json(['results' => $resp]);
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
        }
    }

    public function getUserPackageJson(Request $request)
    {
        try {

            $userList = $this->currentClassRepo->query()->where('id', $request->userId)->first();
            $packageList = UserPackage::where('user_id', $request->userId)->get();
            $resp = [];
            foreach ($packageList as $key => $val) {
                $resp[] = [
                    'id' => $val->id,
                    'text' => $val->package_name
                ];
            }
            return response()->json(['results' => $resp]);
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
        }
    }

}
