<?php

namespace App\Http\Controllers\Backend\DemoCrud;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\DemoCrud\DemoCrudRepository;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class DemoCrudTableController extends Controller
{

    /**
     * @var AdminUserRepository
     */
    protected $demoCrudRepo;

    /**
     * AdminUserTableController constructor.
     * @param AdminUserRepository $demoCrudRepo
     */
    public function __construct(DemoCrudRepository $demoCrudRepo)
    {
        $this->demoCrudRepo = $demoCrudRepo;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        try {
            return DataTables::of($this->demoCrudRepo->getForDataTable())
                ->addIndexColumn()
                ->addColumn('parent_id',function($adminUser){
                    return isset($adminUser->parent_id)
                        ? config('backend.backend.adminUser_type.'.$adminUser->parent_id) : '-';
                })
                ->addColumn('action',function($adminUser){
                    return $adminUser->action_buttons;
                })
                ->addColumn('status',function($adminUser){
                    return $adminUser->status == 1
                        ? '<a href="javascript:void(0)" class="status" data-id="'.$adminUser->id.'"><small class="badge badge-success" data-id="'.$adminUser->id.'">Active</small></a>'
                        : '<a href="javascript:void(0)" class="status" data-id="'.$adminUser->id.'"><small class="badge badge-danger">In-active</small></a>';
                })
                ->rawColumns(['id','action','status'])
                ->make(true);
        }catch (\Exception $ex){
            Log::error($ex->getMessage());
        }
    }
}
