<?php
namespace App\Libs;
class EncryptionData {
    var $ciphering = "AES-128-CTR";
    var $encryption_iv = "1991201820215463"; //16 Character //1991201820215463
    var $encryption_key = "*/@2[znu/U6,Qg-B"; //16 Character
    public function encode($value,$encryptionKey='')
    {
        if($encryptionKey!='')
        {
            $this->encryption_key = $encryptionKey;
        }

        if(!$value)
        {
            return false;
        }

        $options = 0;

        $encryption = openssl_encrypt($value, $this->ciphering, $this->encryption_key, $options, $this->encryption_iv);

        return trim($this->safe_b64encode($encryption));
    }

    public function safe_b64encode($string)
    {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }

    public function decode($value,$encryptionKey='')
    {
        if($encryptionKey!='')
        {
            $this->encryption_key = $encryptionKey;
        }

        if(!$value)
        {
            return false;
        }

        $value = $this->safe_b64decode($value);

        $options = 0;

        $decryption = openssl_decrypt($value, $this->ciphering, $this->encryption_key, $options, $this->encryption_iv);

        return trim($decryption);
    }

    public function safe_b64decode($string)
    {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

}
?>