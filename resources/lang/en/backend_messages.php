<?php

return [
    'global_record_not_found' => 'Record could not be found. Please try again.',
    'global_record_added_failed' => 'Record could not be created. Please try again.',
    'global_record_added_success' => 'Record created successfully',
];
