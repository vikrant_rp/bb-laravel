<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Error</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                {{--<ol class="breadcrumb float-sm-right">
                    <a href="javascript:window.history.back()"
                       class="btn btn-{{config('backend.colors.secondary')}}" style=""> <i class="	fa fa-angle-double-left"></i> Back</a>

                </ol>--}}
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{$pageData->addTitle}}</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <h3> Access Denied </h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>
</section>