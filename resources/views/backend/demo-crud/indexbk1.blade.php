@extends(BACKEND_VIEW.'.layouts.master')

@section('after-styles')
@endsection

@section('title')
    <title>{{ app_name().' | '.$pageData->pageTitle}}</title>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">{{$pageData->mainTitle}}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {!! Breadcrumbs::render() !!}
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content" ng-cloak>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <ui-view></ui-view>

                        <script type="text/ng-template" id="listing.html">
                            @include(BACKEND_VIEW.'.demo-crud.listing')
                        </script>
                        @if(auth()->user()->is_superadmin == 1 || auth()->user()->can('Create Admin-User'))
                        <script type="text/ng-template" id="add.html">
                            @include(BACKEND_VIEW.'.demo-crud.add')
                        </script>
                        @endif
                        {{--<script type="text/ng-template" id="detail.html">
                            <?php $this->load->view('detail'); ?>
                        </script>
                        <script type="text/ng-template" id="update.html">
                            <?php $this->load->view('update'); ?>
                        </script>--}}
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
@endsection

@section('after-scripts')
    <script>
    var urlList = '{{ route("admin.demo-crud.get-list") }}';
    </script>


    <script type="text/javascript" src="{{ asset("assets/common/angularjs/datatable/angular-datatables.js")}}"></script>
    <script type="text/javascript" src="{{ asset("assets/backend/ngJs/demoCrud/index.js".'?t='.time()) }}"></script>
    <script type='text/javascript' src='{{ asset("assets/backend/ngJs/demoCrud/listingCtrl.js".'?t='.time()) }}'></script>

    {{--<script src="https://rawgit.com/michaelbromley/angularUtils-pagination/master/dirPagination.js"></script>--}}
    <script type="text/javascript" src="{{ asset("assets/backend/ngJs/pagination/pager-service.js")}}"></script>


@endsection