@extends(BACKEND_VIEW.'.layouts.master')

@section('after-styles')
@endsection

@section('title')
    <title>{{ app_name().' | '.$pageData->pageTitle}}</title>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">{{$pageData->mainTitle}}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {!! Breadcrumbs::render() !!}
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content" ng-controller="demoCrudController">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">{{$pageData->subTitle}}</h3>
                                {{--<div class="card-tools"
                                     style="position: relative;right: 1rem;top: .5rem;display: inline-flex;float: right;margin-top: -34px;">
                                    {{ Form::select('action',['1'=>'Active','0'=>'In-Active','3' => 'Delete'],null,['class' => 'form-control','style' => 'margin-right:15px','id' => 'bulk-action','placeholder' => 'Select Action']) }}
                                    <a href="javascript:void(0);"
                                       onclick="return deleteAction('admin-user/bulk-action', 'admin-user');"
                                       class="btn btn-primary" style="margin-right: 15px;">Submit</a>
                                    @if(auth()->user()->is_superadmin == 1 || auth()->user()->can('Create Admin-User'))
                                        <a href="{{ route('admin.admin-user.create') }}" title="Create User"
                                           class="btn btn-primary"><i class="fa fa-plus-circle"></i></a>
                                    @endif
                                </div>--}}
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-3">
                                        {{ Form::select('action',['1'=>'Active','0'=>'In-Active','3' => 'Delete'],null,['class' => 'form-control','style' => '','id' => 'bulk-action','placeholder' => 'Select Action']) }}

                                    </div>
                                    <div class="col-sm-3">
                                        <a href="javascript:void(0);"
                                           onclick="return deleteAction('admin-user/bulk-action', 'admin-user');"
                                           class="btn btn-{{config('backend.colors.primary')}}" style="">Submit</a>

                                    </div>
                                    <div class="col-sm-6 ">
                                        <div class="form-group pull-right">
                                            @if(auth()->user()->is_superadmin == 1 || auth()->user()->can('Create Admin-User'))
                                                <a href="{{ route('admin.admin-user.create') }}" title="Create User"
                                                   class="btn btn-{{config('backend.colors.primary')}}"><i class="fa fa-plus-circle"></i> New Record</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table id="users-table" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th><input type="checkbox" id="parent"/></th>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <table datatable="ng" dt-options="vm.dtOptions" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Sr</th>
                                            <th>Customer Name</th>
                                            <th>Address</th>
                                            <th>City</th>
                                            <th>Postal Code</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="customer in customers">
                                            <td>@{{ $index + 1 }}</td>
                                            <td>@{{ customer.ide }}</td>
                                            <td>@{{ customer.name }}</td>
                                            <td>@{{ customer.status }}</td>
                                            <td>@{{ customer.action }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
    </div>
@endsection

@section('after-scripts')
    <script src="http://demo.webslesson.info/angularjs-datatables-php/angular-datatables.min.js"></script>
    <script src="http://demo.webslesson.info/angularjs-datatables-php/jquery.dataTables.min.js"></script>

    <script type='text/javascript' src='{{ asset("assets/backend/ngJs/demoCrud/listController.js".'?t='.time()) }}'></script>
    {{--<script src="{{asset('assets/backend/backend.js')}}" type="application/javascript"></script>--}}
    <script>
        $(function () {
            var oTable = $('#users-table').DataTable({
                processing: false,
                serverSide: true,
                pageLength: 10,
                //stateSave: true,
                ajax: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ route("admin.demo-crud.get-list") }}',
                    type: 'GET',
                    error: function (xhr, err) {
                        if (err === 'parsererror'){
                            //location.reload();
                            alert('Please reload again');
                        }
                    }
                },
                columns: [
                    {
                        data: 'id',
                        name: 'id',
                        render: function (data, type, full, meta) {
                            return '<input type="checkbox" class="child" name="id[]" value="' + data + '">';
                        },
                        sortable: false,
                        searchable: false
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        sortable: true,
                    },
                    {
                        data: 'name',
                        name: 'name',
                        sortable: true,
                        searchable: true
                    },
                    {
                        data: 'status',
                        name: 'status',
                        sortable: true,
                        searchable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                        searchable: false,
                        sortable: false,
                    },
                ],
            });

            $('#users-table').on('click', "#parent", function () {
                $('.child').not(this).prop('checked', this.checked);
            });

            $('#users-table').on('click', '.child', function () {
                if ($('.child:checked').length == $('.child').length) {
                    $('#parent').prop('checked', true);
                } else {
                    $('#parent').prop('checked', false);
                }
            });

            $('#users-table').on('click', '.status', function () {
                var ID = $(this).data('id');
                $.confirm({
                    title: 'Confirm!',
                    content: 'Are you sure to change status?',
                    buttons: {
                        confirm: function () {
                            $.ajax({
                                url: '{{route('admin.admin-user.change-status')}}',
                                data: {'id': ID, _token: '{{ csrf_token() }}'},
                                type: "POST",
                                success: function (data) {
                                    if (data.success == true) {
                                        oTable.draw();
                                    }
                                },
                                error: function (error) {
                                }
                            });
                        },
                        cancel: function () {
                        },
                    }
                });
            });

            $('#users-table').on('click', '.deleteConfirmation', function () {
                var ID = $(this).data('id');
                $.confirm({
                    title: 'Confirm!',
                    content: 'Are you sure to delete this.?',
                    buttons: {
                        confirm: function () {
                            $.ajax({
                                url: '{{route('admin.admin-user.destroy')}}',
                                data: {'id': ID, _token: '{{ csrf_token() }}'},
                                type: "DELETE",
                                success: function (data) {
                                    if (data.success == true) {
                                        oTable.draw();
                                    }
                                },
                                error: function (error) {
                                }
                            });
                        },
                        cancel: function () {
                        },
                    }
                });
            });

        });
    </script>
@endsection
