<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">{{$pageData->mainTitle}}</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					{{--{!! Breadcrumbs::render() !!}--}}
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content" >
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">{{$pageData->subTitle}}</h3>
						{{--<div class="card-tools"
                             style="position: relative;right: 1rem;top: .5rem;display: inline-flex;float: right;margin-top: -34px;">
                            {{ Form::select('action',['1'=>'Active','0'=>'In-Active','3' => 'Delete'],null,['class' => 'form-control','style' => 'margin-right:15px','id' => 'bulk-action','placeholder' => 'Select Action']) }}
                            <a href="javascript:void(0);"
                               onclick="return deleteAction('admin-user/bulk-action', 'admin-user');"
                               class="btn btn-primary" style="margin-right: 15px;">Submit</a>
                            @if(auth()->user()->is_superadmin == 1 || auth()->user()->can('Create Admin-User'))
                                <a href="{{ route('admin.admin-user.create') }}" title="Create User"
                                   class="btn btn-primary"><i class="fa fa-plus-circle"></i></a>
                            @endif
                        </div>--}}
					</div>
					<div class="card-body">
						{{--<div class="row">
								<div class="col-md-3 col-sm-6 col-12">
									<input type="text" class="form-control" placeholder=".col-3">
								</div>
								<div class="col-md-6 col-sm-6 col-12">
									<input type="text" class="form-control" placeholder=".col-4">
								</div>
								<div class="col-md-3 col-sm-6 col-12 ">
									<input type="text" class="form-control" placeholder=".col-5">
								</div>
						</div>--}}
						<div class="row">
							<div class="col-md-6 col-sm-6 col-12"></div>
							<div class="col-md-4 col-sm-6 col-12 mt-1">
								{{ Form::select('filterStatus',_status_array('tests',['0']),null,['class' => 'form-control pull-right select-width','style' => '','id' => 'change_status','placeholder' => 'Filter By Status','ng-change'=>"filterStatusWiseFun()","ng-model"=>"reqData.filterStatus"]) }}
							</div>
							<div class="col-md-2 col-sm-6 col-12 mt-1">
									@if(auth()->user()->is_superadmin == 1 || auth()->user()->can('Create Admin-User'))
										<a href="{{  route('admin.demo-crud.add') }}" title="Create User"
										   class="form-control btn btn-{{config('backend.colors.primary')}}"><i class="fa fa-plus-circle"></i> New Record</a>
									@endif
							</div>

							{{--onclick="return deleteAction('admin-user/bulk-action', 'admin-user');"--}}
							{{--{{ Form::select('action',['1'=>'Active','0'=>'In-Active','3' => 'Delete'],null,['class' => 'form-control','style' => '','id' => 'bulk-action','placeholder' => 'Change Status']) }}--}}
							{{--<div class="col-md-3">
								{{ Form::select('action',_status_array('tests'),null,['class' => 'form-control','style' => '','id' => 'bulk-action','placeholder' => 'Change Status']) }}

							</div>
							<div class="col-md-3">
								<button
										ng-click="bulkActionFun()"
										class="btn btn-{{config('backend.colors.success')}}" style="">Save</button>
							</div>--}}
							{{--<a href="javascript:void(0);"
                               ng-click="reloadData()"
                               class="btn btn-{{config('backend.colors.primary')}}" style="">Test</a>--}}
							{{--<div class="col-md-6"></div>
							<div class="col-md-4 col-sm-12">

								{{ Form::select('filterStatus',_status_array('tests',['0']),null,['class' => 'form-control pull-right select-width','style' => '','id' => 'change_status','placeholder' => 'Filter By Status','ng-change'=>"filterStatusWiseFun()","ng-model"=>"reqData.filterStatus"]) }}
							</div>
							<div class="col-xs-12 col-md-2">
								<div class="form-group pull-right">
									@if(auth()->user()->is_superadmin == 1 || auth()->user()->can('Create Admin-User'))
										--}}{{-- {{route('admin.demo-crud.index')}}--}}{{--
										<a href="#!/add" title="Create User"
										   class="btn btn-{{config('backend.colors.primary')}}"><i class="fa fa-plus-circle"></i> New Record</a>
									@endif
								</div>
							</div>--}}
						</div>
						<div class="row mt-5">
							<table id="list_table_one" class="table table-striped table-bordered table-hover dataTables-example newTab" ng-init="initFun()" >
								<thead>
								<tr>
									{{--<th><input name="select_all" value="1" id="example-select-all" type="checkbox" /></th>--}}
									<th>Sr.#</th>
									<th>Name</th>
									<th>Description</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
								</thead>
								{{--<thead>
								<tr>
									<th><input name="select_all" value="1" id="example-select-all" type="checkbox" /></th>
									<th>Name</th>
									<th>Position</th>
									<th>Office</th>
									<th>Extn.</th>
									<th>Start date</th>
									<th>Salary</th>
								</tr>
								</thead>--}}
								{{--<tfoot>
								<tr>
									<th></th>
									<th>Name</th>
									<th>Position</th>
									<th>Office</th>
									<th>Extn.</th>
									<th>Start date</th>
									<th>Salary</th>
								</tr>
								</tfoot>--}}
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- /.card -->
		</div>
	</div>
</section>