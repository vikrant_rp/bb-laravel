<div class="card">
	<div class="card-header">
		<h3 class="card-title">{{$pageData->subTitle}}</h3>
		{{--<div class="card-tools"
             style="position: relative;right: 1rem;top: .5rem;display: inline-flex;float: right;margin-top: -34px;">
            {{ Form::select('action',['1'=>'Active','0'=>'In-Active','3' => 'Delete'],null,['class' => 'form-control','style' => 'margin-right:15px','id' => 'bulk-action','placeholder' => 'Select Action']) }}
            <a href="javascript:void(0);"
               onclick="return deleteAction('admin-user/bulk-action', 'admin-user');"
               class="btn btn-primary" style="margin-right: 15px;">Submit</a>
            @if(auth()->user()->is_superadmin == 1 || auth()->user()->can('Create Admin-User'))
                <a href="{{ route('admin.admin-user.create') }}" title="Create User"
                   class="btn btn-primary"><i class="fa fa-plus-circle"></i></a>
            @endif
        </div>--}}
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-sm-3">
				{{ Form::select('action',['1'=>'Active','0'=>'In-Active','3' => 'Delete'],null,['class' => 'form-control','style' => '','id' => 'bulk-action','placeholder' => 'Select Action']) }}

			</div>
			<div class="col-sm-3">
				<a href="javascript:void(0);"
				   onclick="return deleteAction('admin-user/bulk-action', 'admin-user');"
				   class="btn btn-{{config('backend.colors.primary')}}" style="">Submit</a>

			</div>
			<div class="col-sm-6 ">
				<div class="form-group pull-right">
					@if(auth()->user()->is_superadmin == 1 || auth()->user()->can('Create Admin-User'))
						<a href="#!/add" title="Create User"
						   class="btn btn-{{config('backend.colors.primary')}}"><i class="fa fa-plus-circle"></i> New Record</a>
					@endif
				</div>
			</div>
		</div>
		<div class="row">
			<table id="entry-grid" datatable="" dt-options="dtOptions" dt-columns="dtColumns" class="table table-hover" dt-column-defs="dtColumnDefs"></table>
			{{--<table class="table table-condensed table-striped table-hover table-bordered row-border hover" datatable="" dt-options="dtOptions" dt-instance="dtInstance" dt-columns="dtColumns" id="dt_pc"></table>--}}
		</div>

		{{--<div class="row">
			<div class="col-sm-5">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text pl-2"><i class="fa fa-search"></i></span>
					</div>
					<input type="text" id="example-input1-group1" name="example-input1-group1" class="form-control pl-1" placeholder="Search Here..." ng-model="reqData.search" ng-keyup='searchFun();'>
				</div>
			</div>
			<div class="col-sm-2">
				<select ng-init="reqData.perPage='10'" ng-model="reqData.perPage" class="form-control" ng-change="perPageFun();">
					<option value="10">10</option>
					<option value="50">50</option>
					<option value="100">100</option>
				</select>
			</div>
			<div class="col-lg-5 text-right">
				<a href="#!/add" class="btn btn-secondary waves-effect w-md m-b-5 float-right"><i class="fa fa-plus"></i> Demo Crud</a>
			</div>
		</div>--}}

		{{--<table class="table productAssignTable">
			<thead>
			<tr>
				<th width="5%"></th>
				<th>Name</th>
				<th>Status</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			<tr ng-repeat="listDataRow in resData.list track by $index">
				<td><span class="avatar-box bg-dark">@{{ listDataRow.name | limitTo:1 }}</span></td>
				<td>@{{listDataRow.name}}</td>
				<td>@{{listDataRow.status}}</td>
				<td class="text-right">
					<a href="#!/update/@{{listDataRow.taxId}}" class="btn btn-round btn-xs btn-secondary"> <i class="fa fa-pencil"></i></a>
					<a href="javascript:void(0)" ng-click="changeStatusFun(listDataRow);"><span ng-if="listDataRow.isActive==1"><i class="fa fa-toggle-on"></i></span><span ng-if="listDataRow.isActive==0"><i class="fa fa-toggle-off"></i></span></a>

				</td>
			</tr>
			<tr ng-show="!reqData.isLoading && !resData.list">
				<td colspan="5" class="inbox-item text-center">
					<div class="alert alert-danger mb-0"><i class="fa-fw fa fa-warning"></i><strong>No data Available</strong></div>
				</td>
			</tr>
			<tr ng-show="reqData.isLoading">
				<td colspan="5" class="inbox-item">Loading.....</td>
			</tr>
			<tr>
				<td colspan="5">
					<ul ng-show='!reqData.isLoading' class="pagination pagination-split mb-0 " ng-if="(pager.pages.length)&&(pager.pages.length>1)&&(pager.totalPages)">
						<li ng-class="{disabled:pager.currentPage === 1}" class="page-item">
							<a href="javascript:void(0);" ng-click="setPageFun(pager.currentPage - 1)" class="page-link"><i class="fa fa-angle-left"></i></a>
						</li>
						<li ng-repeat="page in pager.pages" ng-class="{active:pager.currentPage === page}" class="page-item">
							<a href="javascript:void(0);" ng-click="(pager.currentPage != page) && setPageFun(page)" class="page-link">@{{page}}</a>
						</li>
						<li class="next pag
				e-item" ng-class="{disabled:pager.currentPage === pager.totalPages}">
							<a href="javascript:void(0);" ng-click="setPageFun(pager.currentPage + 1)" class="page-link"><i class="fa fa-angle-right"></i></a>
						</li>
					</ul>
				</td>
			</tr>
			</tbody>
		</table>--}}
	</div>
</div>