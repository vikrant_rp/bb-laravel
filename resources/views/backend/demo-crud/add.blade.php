<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{$pageData->mainTitle}}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <a href="javascript:window.history.back()"
                    class="btn btn-{{config('backend.colors.secondary')}}" style=""> <i class="	fa fa-angle-double-left"></i> Back</a>
                    {{--{!! Breadcrumbs::render() !!}--}}
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{$pageData->addTitle}}</h3>
                        {!!_required_asterisk(1)!!}
                    </div>
                    {{--<div class="alert alert-success" role="alert">
                        {{ __('A fresh verification link has been sent to your email address.') }}
                    </div>--}}
                    {{--{!! Form::open([ 'method' => 'post','files'=>true,'enctype'=>'multipart/form-data','id' => 'addUpdateForm','name'=>'addUpdateForm',"ng-submit"=>"register(addUpdateForm)","ng-validate"=>"validationOptions"]) !!}--}}
                    {{--<form ng-submit="doSomeAction()" >
                        Enter text and hit enter:
                        <input type="text" ng-model="text" name="text" />
                        @include('backend.demo-crud.partials.form')
                        <input type="submit" id="submit" value="Submit" />
                        <pre>list=@{{list}}</pre>
                    </form>--}}
                    {{--{!! Form::open(['method' => 'post', 'id' => 'addUpdateForm','name'=>'addUpdateForm',"ng-submit"=>"submitCrudFrom()"]) !!}--}}
                    <form method="POST" accept-charset="UTF-8" id="addUpdateForm" name="addUpdateForm" ng-submit="formSubmitFun()" ng-validate="validationOptions">
                    {{--<form name="frmFields" class="form-horizontal"      novalidate="">--}}
                    <div class="card-body">
                        @include('backend.demo-crud.partials.form')
                    </div>
                    <div class="card-footer">
                        <a href="#" class="btn btn-default float-left">Cancel</a>
                        <button class="btn btn-primary pull-right" id="adminUserButtonLoader"
                                data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing"
                                type="submit">{{__('buttons.general.crud.create')}}</button>
                        {{--<input type="submit" id="submit" value="Submit" />--}}
                    </div>

                    </form>
                    {{--{!! Form::close() !!}--}}

                    {{--<div class="card-body">
                        <div class="row">
                            <h3> Form Coming Soon</h3>
                            <form name="frmFields" class="form-horizontal"
                                  novalidate="">

                                <div class="form-group error">
                                    <label for="inputEmail3" class="col-sm-12
                                        control-label">Name</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control
                                            has-error" id="name" name="name"
                                               placeholder="Name"
                                               value="[[ name ]]"
                                               ng-model="item.name"
                                               ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmFields.name.$invalid
                                            && frmFields.name.$touched">Name
                                            field is required</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-12
                                        control-label">Quantity</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control"
                                               id="quantity" name="Quantity"
                                               placeholder="quantity"
                                               value="[[ quantity ]]"
                                               ng-model="item.quantity"
                                               ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmFields.quantity.$invalid
                                            && frmFields.quantity.$touched">Quantity field is required</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-12
                                        control-label">Price</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control"
                                               id="price"
                                               name="price"
                                               placeholder="Price"
                                               value="[[ price ]]"
                                               ng-model="item.price"
                                               ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmFields.price.$invalid
                                            &&
                                            frmFields.price.$touched">Price field is required</span>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-secondary"
                                        data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary"
                                        id="btn-save" ng-click="save(modalstate, id)"
                                        ng-disabled="frmFields.$invalid">Save changes</button>
                            </form>

                        </div>
                    </div>--}}
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>
</section>