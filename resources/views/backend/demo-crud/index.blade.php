@extends(BACKEND_VIEW.'.layouts.master')

@section('after-styles')
@endsection

@section('title')
    <title>{{ app_name().' | '.$pageData->pageTitle}}</title>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" ng-cloak>
        {{--<ui-view></ui-view>--}}
        <div ng-view></div>
        <script type="text/ng-template" id="listing.html">
            @include(BACKEND_VIEW.'.demo-crud.listing')
        </script>
        @if(auth()->user()->is_superadmin == 1 || auth()->user()->can('Create Admin-User'))
            <script type="text/ng-template" id="add.html">
                @include(BACKEND_VIEW.'.demo-crud.add')
            </script>
        @endif
        @if(auth()->user()->is_superadmin == 1 || auth()->user()->can('Create Admin-User'))
            <script type="text/ng-template" id="update.html">
                @include(BACKEND_VIEW.'.demo-crud.update')
            </script>
        @endif
        {{--<script type="text/ng-template" id="detail.html">
            <?php $this->load->view('detail'); ?>
        </script>
        <script type="text/ng-template" id="update.html">
            <?php $this->load->view('update'); ?>
        </script>--}}
    </div>
@endsection

@section('after-scripts')
    <script>
        var urlList = '{{ route("admin.demo-crud.get-list") }}';
    </script>
    {{--<script type="text/javascript" src="{{ asset("assets/common/angularjs/datatable/angular-datatables.js")}}"></script>--}}
    {{--<link rel="stylesheet" href="{{ asset("assets/common/ex_plugins/datatables/datatables.min.css")}}">
    <script type="text/javascript" src="{{ asset("assets/common/ex_plugins/datatables/datatables.min.js")}}"></script>
       <link rel="stylesheet" href="{{ asset("assets/common/ex_plugins/datatables/dataTables.checkboxes.css")}}">
    <script type="text/javascript" src="{{ asset("assets/common/ex_plugins/datatables/dataTables.checkboxes.min.js")}}"></script>--}}


    <script type="text/javascript" src="{{ asset("assets/backend/ngJs/demoCrud/index.js".'?t='.time()) }}"></script>
    <script type='text/javascript' src='{{ asset("assets/backend/ngJs/demoCrud/listingCtrl.js".'?t='.time()) }}'></script>
    <script type='text/javascript' src='{{ asset("assets/backend/ngJs/demoCrud/addCtrl.js".'?t='.time()) }}'></script>
    <script type='text/javascript' src='{{ asset("assets/backend/ngJs/demoCrud/updateCtrl.js".'?t='.time()) }}'></script>
    <script type="text/javascript" src="{{ asset("assets/common/ex_plugins/ag-validaiton/jquery.validate.min.js".'?t='.time()) }}"></script>
    <script type="text/javascript" src="{{ asset("assets/common/ex_plugins/ag-validaiton/angular-validate.min.js".'?t='.time()) }}"></script>

    {{--<script src="https://rawgit.com/michaelbromley/angularUtils-pagination/master/dirPagination.js"></script>--}}
    {{--<script type="text/javascript" src="{{ asset("assets/backend/ngJs/pagination/pager-service.js")}}"></script>--}}


@endsection
