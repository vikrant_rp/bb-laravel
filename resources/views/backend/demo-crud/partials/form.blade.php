<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="lable-name">Name{!!_required_asterisk()!!}</label>
            {!! Form::text('name',null, ['class' => 'form-control','placeholder'=>'Enter Name','required1', "ng-model"=>"requestFormData.name"]) !!}
            {{--<span *ngShow="requestFormDataError.name"  id="name-error" class="error invalid-feedback byv" style="">@{{ requestFormDataError.name }}</span>--}}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="lable-name">Description{!!_required_asterisk()!!}</label>
            {!! Form::text('description',null, ['class' => 'form-control','placeholder'=>'Enter Description','required', "ng-model"=>"requestFormData.description"]) !!}
            {{--<span *ngShow="requestFormDataError.description"  id="description-error" class="error invalid-feedback byv" style="">@{{ requestFormDataError.description }}</span>--}}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="lable-name">Price{!!_required_asterisk()!!}</label>
            {!! Form::text('price',null, ['class' => 'form-control','placeholder'=>'Enter Price','required', "ng-model"=>"requestFormData.price"]) !!}
            {{--<span *ngShow="requestFormDataError.price"  id="price-error" class="error invalid-feedback byv" style="">@{{ requestFormDataError.price }}</span>--}}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="lable-type">Status{!!_required_asterisk()!!}</label>
            {!! Form::select('status',_status_array('tests',['0']), null , ['class' => 'form-control select2','id'=>'type','data-placeholder'=>'Select Status', "ng-model"=>"requestFormData.status"]) !!}
        </div>
    </div>
</div>