<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="lable-name">Name{!!_required_asterisk()!!}</label>
            <input type="text" class="form-control has-error" id="name" name="name" placeholder="Name" value="[[ name ]]" ng-model="item.name" ng-required="true">
            <span class="help-inline" ng-show="frmFields.name.$invalid && frmFields.name.$touched">Name field is required</span>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="lable-name">Description{!!_required_asterisk()!!}</label>
            <input type="text" class="form-control"
                   id="description"
                   name="description"
                   placeholder="Enter description"
                   value="[[ description ]]"
                   ng-model="item.description"
                   ng-required="true"
            >
            <span class="help-inline" ng-show="frmFields.description.$invalid && frmFields.description.$touched">Description field is required</span>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="lable-name">Price{!!_required_asterisk()!!}</label>
            <input type="text" class="form-control"
                   id="price"
                   name="price"
                   placeholder="Price"
                   value="[[ price ]]"
                   ng-model="item.price"
                   ng-required="true"
                    >
            <span class="help-inline" ng-show="frmFields.price.$invalid && frmFields.price.$touched">Price field is required</span>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="lable-type">Status{!!_required_asterisk()!!}</label>
            {!! Form::select('status',_status_array('tests',['0']), null , ['class' => 'form-control select2','id'=>'type','data-placeholder'=>'Select Status']) !!}
        </div>
    </div>
</div>