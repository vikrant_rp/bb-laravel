<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @yield('title')
    <base href="{{url('/')}}/bbadmin/">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        var CSRFTOKEN = '{{ csrf_token() }}';
    </script>
    {{-- Include styles files--}}
    @include(BACKEND_VIEW.'.layouts.styles')
    @yield('after-styles')
</head>
<body class="hold-transition sidebar-mini" ng-app="myApp">
<div class="wrapper">
    {{--Include Navbar --}}
    @include(BACKEND_VIEW.'.layouts.header')
    <!-- Main Sidebar Container -->
    @include(BACKEND_VIEW.'.layouts.sidebar')
    @yield('content')
    <!-- Footer Container-->
    @include(BACKEND_VIEW.'.layouts.footer')
</div>
<!-- ./wrapper -->

{{--Include scripts files--}}
@include(BACKEND_VIEW.'.layouts.scripts')
@yield('after-scripts')
</body>
</html>
