<!-- jQuery -->
<script src="{{config('site-config.backend_assets_url')}}/plugins/jquery/jquery.min.js"></script>

<!-- jQuery UI 1.11.4 -->
<script src="{{asset('js/jquery-ui.min.js')}}"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>

<!-- Bootstrap 4 -->
<script src="{{config('site-config.backend_assets_url')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<script src="{{config('site-config.backend_assets_url')}}/plugins/morris/morris.min.js"></script>

<!-- Sparkline -->
<script src="{{config('site-config.backend_assets_url')}}/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- jvectormap -->
<script src="{{config('site-config.backend_assets_url')}}/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{config('site-config.backend_assets_url')}}/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- jQuery Knob Chart -->
<script src="{{config('site-config.backend_assets_url')}}/plugins/knob/jquery.knob.js"></script>

<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="{{config('site-config.backend_assets_url')}}/plugins/daterangepicker/daterangepicker.js"></script>

<!-- datepicker -->
<script src="{{config('site-config.backend_assets_url')}}/plugins/datepicker/bootstrap-datepicker.js"></script>

<!-- Bootstrap WYSIHTML5 -->
<script src="{{config('site-config.backend_assets_url')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<!-- Slimscroll -->
<script src="{{config('site-config.backend_assets_url')}}/plugins/slimScroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="{{config('site-config.backend_assets_url')}}/plugins/fastclick/fastclick.js"></script>

<!-- Select2 -->
<script src="{{config('site-config.backend_assets_url')}}/plugins/select2/select2.full.min.js"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="{{config('site-config.backend_assets_url')}}/dist/js/pages/dashboard.js"></script>--}}

<!-- AdminLTE for demo purposes -->
<script src="{{config('site-config.backend_assets_url')}}/dist/js/demo.js"></script>

<!-- iCheck -->
<script src="{{config('site-config.backend_assets_url')}}/plugins/iCheck/icheck.min.js"></script>

<!-- AdminLTE App -->
<script src="{{config('site-config.backend_assets_url')}}/dist/js/adminlte.js"></script>

<!-- DataTables -->
<script src="{{ config('site-config.backend_assets_url')}}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ config('site-config.backend_assets_url')}}/plugins/datatables/dataTables.bootstrap4.js"></script>
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/js/dataTables.checkboxes.min.js"></script>

<!-- Toastr -->
@toastr_js
@toastr_render

<!-- Dropzone-->
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>--}}

<!-- Jquery Validate -->
<script src="{{ asset('js')}}/jquery.validate.min.js"></script>

<!-- Sweet Alert2 JS-->
{{--<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/5.0.7/sweetalert2.min.js" type="text/javascript"></script>--}}
<!-- Jquery Confirmation CSS-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<!-- CK Editor -->
<script src="{{ config('site-config.backend_assets_url')}}/plugins/ckeditor/ckeditor.js"></script>
<script>
    jQuery.validator.addMethod("noSpace", function(value, element) {
        return value == '' || value.trim().length != 0;
    }, "No space please and don't leave it empty");

    $(document).ready(function(){
        $('.select2').select2({allowClear:true})
    });
    var baseURLJS = '<?= backend_base_url ?>';
</script>
<script src="{{asset('assets/backend/backend.js')}}" type="application/javascript"></script>
<!-- Multiple Image Upload JS-->
<script src="{{asset('js/fileinput.js')}}" type="text/javascript"></script>
<script src="{{asset('js/theme.js')}}" type="text/javascript"></script>
<script src="{{asset('js/popper.min.js')}}" type="text/javascript"></script>

<!-- Addmore Jquery Plugin JS -->
<script src="{{asset('js/jquery.fieldsaddmore.min.js')}}" type="text/javascript"></script>

<!-- Custom Validation JS -->
<script src="{{asset('js/validation.js')}}" type="text/javascript"></script>

{{--<link rel="stylesheet" type="text/css" href="{{ asset('assets/common/ex_plugins/jquery-confirm-master/dist/jquery-confirm.min.css') }}">
<script type="text/javascript" src="{{ asset('assets/common/ex_plugins/jquery-confirm-master/dist/jquery-confirm.min.js') }}"></script>--}}


{{--<script type="text/javascript" src="{{ asset("assets/common/angularjs/angular.min.js")}}"></script>
<script type="text/javascript" src="{{ asset("assets/common/angularjs/underscore-min.js")}}"></script>--}}
{{--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-route.js"></script>--}}
{{--<script type="text/javascript" src="{{ asset("assets/common/angularjs/angular-ui-router.min.js")}}"></script>--}}
{{--<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-utils/0.1.1/angular-ui-utils.min.js"></script>--}}
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-route.js"></script>


<link rel="stylesheet" type="text/css" href="{{ asset('assets/common/ex_plugins/jquery-confirm-master/bb/angular-confirm.css') }}">
<script type="text/javascript" src="{{ asset('assets/common/ex_plugins/jquery-confirm-master/bb/angular-confirm.js') }}"></script>
{{--<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.9/angular.min.js"></script>--}}
{{--<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular-route.min.js"></script>--}}
{{--<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular-resource.min.js"></script>--}}
{{--<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-sanitize.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment-range/2.2.0/moment-range.min.js"></script>--}}
<script type='text/javascript' src='{{ asset("assets/backend/ngJs/main.js".'?t='.time()) }}'></script>
<script type='text/javascript' src='{{ asset("assets/backend/ngJs/bbJSNotify.js".'?t='.time()) }}'></script>
