<!-- Font Awesome -->
<link rel="stylesheet" href="{{config('site-config.backend_assets_url')}}/plugins/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="{{config('site-config.backend_assets_url')}}/plugins/select2/select2.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="{{config('site-config.backend_assets_url')}}/plugins/iCheck/flat/blue.css">
<!-- Morris chart -->
<link rel="stylesheet" href="{{config('site-config.backend_assets_url')}}/plugins/morris/morris.css">
<!-- jvectormap -->
<link rel="stylesheet" href="{{config('site-config.backend_assets_url')}}/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Date Picker -->
<link rel="stylesheet" href="{{config('site-config.backend_assets_url')}}/plugins/datepicker/datepicker3.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{config('site-config.backend_assets_url')}}/plugins/daterangepicker/daterangepicker-bs3.css">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{config('site-config.backend_assets_url')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<!-- iCheck -->
<link rel="stylesheet" href="{{config('site-config.backend_assets_url')}}/plugins/iCheck/square/blue.css">

<!-- Dropzone -->
<link rel="stylesheet" href="{{config('site-config.backend_assets_url')}}/plugins/dropzone/dropzone.min.css">

<!-- Theme style -->
<link rel="stylesheet" href="{{config('site-config.backend_assets_url')}}/dist/css/adminlte.min.css">

<!-- DataTables -->
{{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">--}}
<link rel="stylesheet" href="{{config('site-config.backend_assets_url')}}/plugins/datatables/dataTables.bootstrap4.css">
<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/css/dataTables.checkboxes.css" rel="stylesheet" />

<!-- Toastr -->
@toastr_css
<!-- Common  -->
{{--<link rel="stylesheet" href="{{asset('css')}}/common.css">--}}
<link href="{{ asset('assets/backend/custom.css') }}" rel="stylesheet">


<!-- Image Upload CSS-->
<link href="{{asset('css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>

<!-- Sweet Alert2 CSS-->
{{--<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/5.0.7/sweetalert2.min.css" type="text/css">--}}
<!-- Jquery Confirmation CSS-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

<!-- Image Upload CSS-->
<style>
    .fileinput-cancel {
        display: none;
    }

    .file-caption-main {
        display: none;
    }
</style>