<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{app_name().' | Log in'}}</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{config('site-config.backend_assets_url')}}/dist/css/adminlte.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{config('site-config.backend_assets_url')}}/plugins/iCheck/square/blue.css">
    <link href="{{ url('assets/backend/custom.css') }}" rel="stylesheet">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ _get_favicon() }}">
</head>
<body class="hold-transition register-page">
<div id="app">
        @yield('content')
</div>
<!-- jQuery -->
<script src="{{config('site-config.backend_assets_url')}}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{config('site-config.backend_assets_url')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
{{-- Validation --}}
<script src="{{ asset('assets/common/ex_plugins/jquery-validation-1.19.1/dist/jquery.validate.js') }}"></script>

@section("page_vendors")
@show
@section("page_script")
@show
</body>

</html>
