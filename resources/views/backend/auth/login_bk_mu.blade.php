@extends(BACKEND_VIEW.'.layouts.app_auth')

@section("title", "Admin Login")

@section("content")
<div class="login-box">
    <div class="login-logo">
        <a href="{{route('admin.auth.login')}}"><b>{{app_name()}} Login </b></a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            @if($errors->first('active'))
                <div class="alert alert-danger" role="alert">
                    <strong>{{ $errors->first('active') }}</strong>
                </div>
            @endif
            <p class="login-box-msg">Sign in to Admin Panel</p>
            {!! Form::open(['route' => 'admin.auth.login.post', 'method' => 'post','id'=>'bb_login_form']) !!}
            <div class="form-group has-feedback">
                <input type="email" name="email" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                       placeholder="Email" value="{{old('email')}}">
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password"
                       class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password">
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="row">
                <div class="col-8">
                </div>
                <!-- /.col -->
                <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}

            <p class="mb-1">
                <a href="{{ route('admin.auth.password.request') }}">Forgot Your Password?</a>
            </p>
            <p class="mb-0">
                <a href="{{ route('admin.auth.register') }}" class="text-center">Register</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->
<!-- iCheck -->
@endsection
@section("page_script")
<script src="{{config('site-config.backend_assets_url')}}/plugins/iCheck/icheck.min.js"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        })
    })
    $(document).ready(function(){
        $('#bb_login_form').validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                    //minlength: 5
                }

            },
            messages: {
                email: {
                    required: '{{ __('validation.required',['attribute'=>'email']) }}',
                    email: '{{ __('validation.email',['attribute'=>'email']) }}'
                },
                password: {
                    required: '{{ __('validation.required',['attribute'=>'password']) }}',
                    //minlength: '{{ __('validation.min.string',['attribute'=>'password','min'=>7]) }}'
                },
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });
</script>
@endsection