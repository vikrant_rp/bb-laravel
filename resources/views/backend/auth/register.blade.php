@extends(BACKEND_VIEW.'.layouts.app_auth')

@section("title", "Admin Login")

@section("content")
<div class="register-box">
    <div class="register-logo">
        <a href="{{route('admin.auth.register')}}"><b>{{app_name()}}  Register</b></a>
    </div>

    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a href="{{route('admin.auth.login')}}"><b>{{app_name()}} </b></a>
        </div>
        <div class="card-body register-card-body">
            <p class="login-box-msg">Fill up the form for registration</p>

            {!! Form::open(['route' => 'admin.auth.register.post', 'method' => 'post']) !!}
            <div class="form-group has-feedback">
                <input id="name" type="text"
                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                       name="name" value="{{ old('name') }}" placeholder="{{__('Name')}}" autofocus>
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input id="email" type="email"
                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                       name="email" value="{{ old('email') }}" placeholder="{{ __('E-Mail Address') }}">
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input id="password" type="password"
                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                       name="password" placeholder="{{ __('Password') }}">
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input id="password-confirm" type="password" class="form-control"
                       name="password_confirmation" placeholder="{{ __('Confirm Password') }}">
            </div>
            <div class="row">
                <div class="col-8">
                    {{--<div class="checkbox icheck">--}}
                    {{--<label>--}}
                    {{--<input type="checkbox"> I agree to the <a href="#">terms</a>--}}
                    {{--</label>--}}
                    {{--</div>--}}
                </div>
                <!-- /.col -->
                <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}
            <a href="{{route('admin.auth.login')}}" class="text-center">Sign In</a>
        </div>
        <!-- /.form-box -->
    </div><!-- /.card -->
</div>
<!-- /.register-box -->

@endsection
@section("page_script")
    <script src="{{config('site-config.backend_assets_url')}}/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            })
        })
        $(document).ready(function(){
            $('#bb_login_form').validate({
                rules: {
                    email: {
                        required: true,
                        email: true,
                    },
                    password: {
                        required: true,
                        //minlength: 5
                    }

                },
                messages: {
                    email: {
                        required: '{{ __('validation.required',['attribute'=>'email']) }}',
                        email: '{{ __('validation.email',['attribute'=>'email']) }}'
                    },
                    password: {
                        required: '{{ __('validation.required',['attribute'=>'password']) }}',
                        //minlength: '{{ __('validation.min.string',['attribute'=>'password','min'=>7]) }}'
                    },
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>
@endsection
