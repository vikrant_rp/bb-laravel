//var app = angular.module('pointepayusApp', ['ui.router']);
//var app = angular.module('pointepayusApp', ['ui.router']);
app.config(function($routeProvider) {
    $routeProvider
        .when("/listing", {
            templateUrl: 'listing.html',
            controller: 'listingCtrl'
        })
        .when("/red", {
            templateUrl : "red.htm"
        })
        .when("/green", {
            templateUrl : "green.htm"
        })
        .when("/blue", {
            templateUrl : "blue.htm"
        });
});

app.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/listing');
    $stateProvider.state('listing', {
        url: '/listing',
        templateUrl: 'listing.html',
        controller: 'listingCtrl'
    })
    .state('add', {
        url: '/add',
        templateUrl: 'add.html',
        controller: 'addCtrl',
    })
    .state('update', {
        url: '/update/:id',
        templateUrl: 'update.html',
        controller: 'updateCtrl',
        resolve: {
            check: ($stateParams, $location) => {
                if ($stateParams.id=='') {
                    $location.path('/listing');
                }
            }
        }
    });
    /*.state('detail', {
        url: '/detail/:taxId',
        templateUrl: 'detail.html',
        controller: 'detailCtrl',
        resolve: {
            check: ($stateParams, $location) => {
                if (!parseInt($stateParams.taxId)) {
                    $location.path('/taxId');
                }
            }
        }
    }).state('update', {
        url: '/update/:taxId',
        templateUrl: 'update.html',
        controller: 'updateCtrl',
        resolve: {
            check: ($stateParams, $location) => {
                if (!parseInt($stateParams.taxId)) {
                    $location.path('/listing');
                }
            }
        }
    });*/
});