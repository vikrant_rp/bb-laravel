var moduleName = '/demo-crud/';
app.config(function($locationProvider,$routeProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider
        .when(moduleName +"list", {
            templateUrl: 'listing.html',
            controller: 'listingCtrl'
        })
        .when(moduleName +"add", {
            templateUrl: 'add.html',
            controller: 'addCtrl'
        })
        .when("/green", {
            templateUrl : "green.htm"
        })
        .when("/blue", {
            templateUrl : "blue.htm"
        })

        .when('update', {
            url: '/update/:id',
            templateUrl: 'update.html',
            controller: 'updateCtrl',
            resolve: {
                check: ($stateParams, $location) => {
                if ($stateParams.id=='') {
        $location.path('/listing');
    }
}
}
}) .otherwise({
    redirectTo: moduleName +'list'
});
});
