app.controller("addCtrl", function ($scope, $http, swalAlertService) {

    $scope.formData = { isLoading: false, taxName: '', };
    $scope.formError = false;

    $scope.addTaxFun = () => {
        $scope.formError = false;
        $scope.formData.isLoading = true;
        swalAlertService.loadingStart();

        $http({
            method: "POST",
            url: base_url + 'supermerchant/tax/add',
            data: $.param($scope.formData),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(success => {
            response = success.data;
            swalAlertService.loadingStop();
            if (response.success == 1) {
                swalAlertService.successWithCallback(response.message, () => {
                    window.location.href = `${base_url}supermerchant/tax`;
                });
            }
            else {
                $scope.formData.isLoading = false;
                if (typeof response.message == 'string') {
                    swalAlertService.errorAlert(response.message);
                }
                else {
                    $scope.formError = response.message;
                }
            }
        }, error => {
            console.log(error);
        });
    }
});