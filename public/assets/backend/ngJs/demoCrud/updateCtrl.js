/*app.config(function ($validatorProvider) {
 $validatorProvider.setDefaults({
 errorElement: 'span',
 errorClass: 'help-block'
 });
 });*/
app.controller('addCtrl', ['$scope','$routeParams','$http','$filter', '$window', '$location', '$anchorScroll','$timeout','config','bbNgNotify', function($scope, $routeParams,$http, $filter, $window, $location, $anchorScroll,$timeout,config,bbNgNotify) {
    console.log($routeParams);
    $scope.id = $routeParams.id;
    $scope.apiURL = config.apiUrl+moduleName;
    $scope.reqData = {filterStatus: ''};
    $scope.requestFormData = {name:'',description:'',price:'',status:'1'};
    $scope.requestFormDataError = {};
    $scope.responseList = {};
    $scope.testTxt = "TETS111";
    console.log(config);
    $scope.validationOptions = {
        rules: {
            price: {
                required: true,
                number : true
            },
            description: {
                required: true,
                minlength: 5,
                maxlength: 100
            },
            name: {
                required: true,
                //minlength: 2
            },
            status: {
                required: true,
            }
        },
        messages: {
            /*email: {
             required: "We need your email address to contact you",
             email: "Your email address must be in the format of name@domain.com"
             },
             password: {
             required: "You must enter a password",
             minlength: "Your password must have a minimum length of 6 characters"
             }*/
            name: {
                required: "The name field is required.",
                //minlength: "Your password must have a minimum length of 6 characters"
            },
            description: {
                required: "The description field is required.",
                maxlength: "The description must be at least 255 characters."
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    };
    //console.log('$location',$location.$$url);
    //console.log($location.$$absUrl.replace($location.$$url, '/'));
    $scope.formSubmitFun = function() {
        //bbNgNotify.notifyErrorAutoClose('ererer er aer aer','Error'); return false;
        //window.history.back();
        $scope.requestFormDataError = false;
        //$event.preventDefault();
        console.log($scope.addUpdateForm.validate());
        if ($scope.addUpdateForm.validate()) {
            $scope.recordUpdateFun();
        }else{
            return false;
        }
    };

    $scope.recordUpdateFun = function(){
        $http({
            method  : 'POST',
            url     : $scope.apiURL+'record-update/'+$scope.id,
            headers: {
                //'X-CSRF-TOKEN': CSRFTOKEN
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                //'_token': $('meta[name="csrf-token"]').attr('content'),
                //'Content-Type': 'application/x-www-form-urlencoded'
            },
            data    : $scope.requestFormData
        }).then(function(response) { //handle Success scenario
            var responseData = response.data;
            if(responseData.status){
                bbNgNotify.notifySuccessAutoClose(responseData.message);
                window.history.back();
                //bbNotification.successRedirect(responseData.message,$window.urlReview);
            }else{
                $scope.showErrorFun(response);
                //bbNotification.error(responseData.message);
            }
        },function(response){ //Only Handle Error Scenario
            var responseData = response.data;
            if(response.status ==400){
                $scope.showErrorFun(response);
            }else
            if(response.status !=200){
                $scope.showErrorFun(response);
                //bbNotification.error(responseData.message);
            }
        }).finally(function() {
        });
    };
    $scope.showErrorFun = function (response){
        if(angular.isObject(response.data.message)){
            //throw {message: response.data};
            //$log.error(response.status,response.data);
            alert("object");
        }else{
            bbNgNotify.notifyErrorAutoClose(response.data.message,'Error');
            //$scope.formError = response.data.message;
            //bbNotification.error(response.data.message);
        }
    };

    $scope.detail = false;
    $scope.getDetailFun = () => {
        $scope.detail = false;
        $http({
            method  : 'GET',
            url     : $scope.apiURL+'get-detail/'+$scope.id,
            headers: {
                //'X-CSRF-TOKEN': CSRFTOKEN
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                //'_token': $('meta[name="csrf-token"]').attr('content'),
                //'Content-Type': 'application/x-www-form-urlencoded'
            },
            //params    : {id:$scope.id},
        }).then(function(response) { //handle Success scenario
            var responseData = response.data;
            if(responseData.status){
                //bbNgNotify.notifySuccessAutoClose(responseData.message);
                $scope.requestFormData = responseData.data;
            }else{
                $scope.showErrorFun(response);
               // $location.path('/list' );
                //window.history.back();
            }
        },function(response){ //Only Handle Error Scenario
            var responseData = response.data;
            if(response.status ==404){
                $scope.showErrorFun(response);
            }else
            if(response.status !=200){
                $scope.showErrorFun(response);
                //bbNotification.error(responseData.message);
            }
            //window.history.back();
            //$location.path('/list' );
        }).finally(function() {
        });
    }
    $scope.getDetailFun();


}
]);


app.controller("updateCtrl1", function ($scope, $http, $stateParams) {
    $scope.formData = { isLoading: false, taxId: $stateParams.id, taxName: ''};
    $scope.formError = false;
    console.log($scope.formData);
   /* $scope.updateUserFun = () => {
        $scope.formError = false;
        $scope.formData.isLoading = true;
        swalAlertService.loadingStart();

        $http({
            method: "POST",
            url: base_url + 'supermerchant/tax/update',
            data: $.param($scope.formData),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(success => {
            response = success.data;
            swalAlertService.loadingStop();
            if (response.success == 1) {
                swalAlertService.successWithCallback(response.message, () => {
                    window.location.href = `${base_url}supermerchant/tax`;
                });
            }
            else {
                $scope.formData.isLoading = false;
                if (typeof response.message == 'string') {
                    swalAlertService.errorAlert(response.message);
                }
                else {
                    $scope.formError = response.message;
                }
            }
        }, error => {
            console.log(error);
        });
    }

    $scope.detail = false;
    $scope.detailFun = () => {
        $scope.detail = false;
        $http({
            method: "POST",
            url: base_url + 'supermerchant/tax/detail',
            data: $.param({
                taxId: $stateParams.taxId
            }),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(success => {
            response = success.data;
            $scope.detail = response;
            $scope.formData.taxName = response.taxName;

        }, error => {
            console.log(error);
        });
    }

    $scope.detailFun();
*/


});