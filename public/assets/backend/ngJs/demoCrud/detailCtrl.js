app.controller("detailCtrl", function ($scope, $http, $stateParams) {
    $scope.detail = false;
    $scope.reqData = { isLoading: false, employeeId: $stateParams.employeeId };

    $scope.detailFun = () => {
        $scope.detail = false;
        $scope.reqData.isLoading = true;

        $http({
            method: "POST",
            url: base_url + 'partner/employee/detail',
            data: $.param($scope.reqData),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(success => {
            response = success.data;
            $scope.reqData.isLoading = false;
            $scope.detail = response;
        }, error => {
            console.log(error);
        });
    }

    $scope.detailFun();
});