app.controller('listingCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnBuilder','DTColumnDefBuilder',
    function ($scope, $http, DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder) {
        //var canceler = $q.defer();
        //$scope.pager = {};
        //$scope.reqData = { isLoading: true, perPage: 10, page: 1, search: '' };
        $scope.requestData = {};
        //$scope.listData = [];
        $scope.dtInstance = {};
        $scope.instances = [];
        $scope.dtColumns = [
            //here We will add .withOption('name','column_name') for send column name to the server
            DTColumnBuilder.newColumn("id", "Sr.#").withOption('name', 'id'),
            DTColumnBuilder.newColumn("name", "Name").withOption('name', 'name'),
            DTColumnBuilder.newColumn("price", "Price").withOption('name', 'price'),
            DTColumnBuilder.newColumn("status", "Status").withOption('name', 'status'),
            DTColumnBuilder.newColumn("action", "Action").withOption('name', 'action'),
        ];
        $scope.dtColumnDefs = [
            DTColumnDefBuilder.newColumnDef(0).notSortable(),
            DTColumnDefBuilder.newColumnDef(3).notSortable(),
            DTColumnDefBuilder.newColumnDef(4).notSortable()
        ];
        $scope.fetchList =()  => {
            //$scope.reqData.isLoading = true;
            //canceler.resolve();
            //canceler = $q.defer();
            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                /*dataSrc: "data",
                 url: urlList,
                 type:"POST"*/

                type: 'POST',
                url: urlList,
                //contentType: 'application/json',
                //processData: false,
                //beforeSend: function(xhr, settings) {
                //...
                //},
                //data: function(data) {
                //...
                //}
                headers: {
                    'X-CSRF-TOKEN': CSRFTOKEN
                },
                data: function(d) {
                    d.limit = d.length;
                },
                dataSrc: function(json) {
                    //console.log('SUCCESS')
                    //console.log(json.data)
                    //$scope.listData = json.data;
                    return json.data
                },
                //complete still works
                complete: function(jqXHR, textStatus) {
                    //  console.log(jqXHR.responseText)
                }
            })
                .withOption('processing', true) //for show progress bar
                .withOption('serverSide', true) // for server side processing
                .withPaginationType('full_numbers') // for get full pagination options // first / last / prev / next and page numbers
                ///.withDisplayLength(10) // Page size
                //.withOption('aaSorting',[1,'asc']) // for default sorting column // here 0 means first column
                .withOption('order',[]) // for default sorting column // here 0 means first column
        }

        $scope.fetchList();
        $scope.reloadData = function() {
            $scope.requestData.searchByStatus = 'Active';
            $scope.dtInstance.rerender();
        }

    }
]);
