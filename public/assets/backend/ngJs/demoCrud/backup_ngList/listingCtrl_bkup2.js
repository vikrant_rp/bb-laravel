//var app = angular.module('MyApp', ['datatables']);
app.controller('listingCtrlx', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnBuilder','DTColumnDefBuilder',
    function ($scope, $http, DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder) {
        //var canceler = $q.defer();

        $scope.vm = {};
        $scope.vm.dtInstance = {};
        $scope.vm.dtColumnDefs = [
            DTColumnDefBuilder.newColumnDef(2).notSortable(),
            DTColumnDefBuilder.newColumnDef(3).notSortable()
        ];
        $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('paging', true)
            .withOption('searching', false)
            .withOption('info', false);

        /*$scope.getData1 = function() {
            $http.get("http://dummy.restapiexample.com/api/v1/employees")
                .then(function(response){
                    $scope.userList = response.data;
                    console.log(response.data);
                });
        }*/
        $scope.getData2 = function() {
            /*$http.get("http://jsonplaceholder.typicode.com/posts")
                .then(function(response){
                    $scope.userList1 = response.data;
                    console.log(response.data);
                });*/
            $http({
                method: "POST",
                // url: base_url + 'supermerchant/tax/ajaxList',
                url: 'https://angular-datatables-demo-server.herokuapp.com/',
                data: $.param($scope.reqData),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                //timeout: canceler.promise
            }).then(success => {
                response = success.data;
                console.log(response )
                $scope.userList = response.data.data;

            }, error => {
                console.log(error);
            });

        }
        //$scope.getData1();
        $scope.getData2();

    }
]);
app.controller('listingCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnBuilder','DTColumnDefBuilder',
    function ($scope, $http, DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder) {
        //var canceler = $q.defer();

        $scope.pager = {};
        $scope.reqData = { isLoading: true, perPage: 10, page: 1, search: '' };
        $scope.resData = { list: false };
        $scope.listData = [];
        $scope.dtInstance = {};
        $scope.instances = [];
        $scope.dtColumns = [
            //here We will add .withOption('name','column_name') for send column name to the server
            DTColumnBuilder.newColumn("id", "Sr.#").withOption('name', 'id'),
            DTColumnBuilder.newColumn("name", "Name").withOption('name', 'name'),
            DTColumnBuilder.newColumn("price", "Price").withOption('name', 'price'),
            DTColumnBuilder.newColumn("status", "Status").withOption('name', 'status'),
            DTColumnBuilder.newColumn("action", "Action").withOption('name', 'action'),
        ];
        $scope.dtColumnDefs = [
            DTColumnDefBuilder.newColumnDef(0).notSortable(),
            DTColumnDefBuilder.newColumnDef(3).notSortable(),
            DTColumnDefBuilder.newColumnDef(4).notSortable()
        ];
        $scope.fetchList =()  => {
            $scope.reqData.isLoading = true;
            //canceler.resolve();
            //canceler = $q.defer();
            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                /*dataSrc: "data",
                 url: urlList,
                 type:"POST"*/

                type: 'get',
                url: urlList,
                //contentType: 'application/json',
                //processData: false,
                //beforeSend: function(xhr, settings) {
                //...
                //},
                //data: function(data) {
                //...
                //}
                dataSrc: function(json) {
                    //console.log('SUCCESS')
                    //console.log(json.data)
                    //$scope.listData = json.data;
                    return json.data
                },
                //complete still works
                complete: function(jqXHR, textStatus) {
                    //  console.log(jqXHR.responseText)
                }
            })
                .withOption('processing', true) //for show progress bar
                .withOption('serverSide', true) // for server side processing
                .withPaginationType('full_numbers') // for get full pagination options // first / last / prev / next and page numbers
                ///.withDisplayLength(10) // Page size
                //.withOption('aaSorting',[1,'asc']) // for default sorting column // here 0 means first column
                .withOption('order',[]) // for default sorting column // here 0 means first column
        }

        $scope.fetchList();

    }
]);

app.controller("listingCtrl1", function (PagerService, $scope, $http, $q) {
    //,swalAlertService
    var canceler = $q.defer();

    $scope.pager = {};
    $scope.reqData = { isLoading: true, perPage: 10, page: 1, search: '' };
    $scope.resData = { list: false };

    $scope.setPageFun = page => {
        canceler.resolve();
        canceler = $q.defer();

        if (page < 1 || page > $scope.pager.totalPages) {
            return;
        }

        $scope.reqData.isLoading = true;
        $scope.reqData.page = page;
        $scope.resData.list = false;

        $http({
            method: "POST",
            // url: base_url + 'supermerchant/tax/ajaxList',
            url: 'https://angular-datatables-demo-server.herokuapp.com/',
            data: $.param($scope.reqData),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            timeout: canceler.promise
        }).then(success => {
            response = success.data;
        $scope.reqData.isLoading = false;
        $scope.resData.list = response.list;
        $scope.pager = PagerService.GetPager(response.total, page, $scope.reqData.perPage);
    }, error => {
        console.log(error);
    });
};

$scope.perPageFun = () => {
    $scope.pager = {};
    $scope.resData.list = false;
    $scope.setPageFun(1);
};

$scope.searchFun = () => {
    $scope.pager = {};
    $scope.resData.list = false;
    $scope.setPageFun(1);
};

$scope.perPageFun();

$scope.changeStatusFun = taxObj => {
    var type =  (taxObj.isActive==1)? 'In-Active': 'Active';
    swal({
        title: '',
        text: 'Are You Sure You Want To '+type+' this tax type?',
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false,
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false
    }).then(function (isConfirm,) {
        if (isConfirm) {
            //swalAlertService.loadingStart();
            $http({
                method: "POST",
                url: base_url + 'supermerchant/tax/changeStatus',
                data: $.param({
                    taxId: taxObj.taxId,
                    isActive: taxObj.isActive
                }),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                timeout: canceler.promise
            }).then(success => {
                response = success.data;
            console.log(response);
            //swalAlertService.loadingStop();
            /*if (response.success == 1) {
             swalAlertService.successWithCallback(response.message, () => {
             window.location.href = `${base_url}supermerchant/tax`;
             });
             } else {
             swalAlertService.errorAlert(response.message);
             }*/

        }, error => {
            console.log(error);
        });

    }
else {
        swal("Cancelled", "", "error");
    }
}, function (dismiss) {
    if (dismiss === 'cancel') {
        swal("Cancelled", "", "error");
    }
});
};


});