
app.controller('listingCtrl', ['$scope','$http','$filter', '$window', '$location', '$anchorScroll','$timeout', function($scope, $http, $filter, $window, $location, $anchorScroll,$timeout){
    $scope.reqData = { filterStatus: '',aList:[]};
    $scope.responseList = {};
    $scope.listTable = [];
    $scope.checkAll = false;
    $scope.initFun = function(){
        $scope.getListFun();
    };
    $scope.getListFun = function(){

        $scope.listTable = jQuery('#list_table_one').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            //stateSave: true,
            "order": [], //Initial no order.
            // Load data for the table's content from an Ajax source
            "ajax": {
                //"url": 'https://angular-datatables-demo-server.herokuapp.com/',
                "url": urlList,
                "type": "POST",
                headers: {
                    //'X-CSRF-TOKEN': CSRFTOKEN
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    //'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: function (data) { //Filter Option
                    data.filters = $scope.reqData;
                    //data.user_name = $('#user_name').val();
                },
                error: function (xhr, err) {
                    if (err === 'parsererror')
                        alert("error");
                }
                //data: $scope.reqData
                /*"url": "{{ url('allposts') }}",
                 "dataType": "json",
                 "type": "POST",
                 "data":{ _token: "{{csrf_token()}}"}*/
            },
            //Set column definition initialisation properties.
            /*"columnDefs": [
             {
             "targets": 0,//[0,1,2,3,4], //first column / numbering column
             "orderable": false, //set not orderable
             'checkboxes': {
             'selectRow': true
             }
             }
             ],*/
            "columnDefs": [
                /*{
                    "targets": 0,//[0,1,2,3,4], //first column / numbering column
                    "orderable": false, //set not orderable
                    'searchable':false,
                    'checkboxes': {
                        'selectRow': true
                    },
                    //'className': 'example-select-all',
                    'className': 'dt-body-center',
                    'render': function(data, type, full, meta) {
                        return '<input type="checkbox" name="id[]" value="' +
                            $('<div/>').text(data).html() + '">';
                    }
                },*/
                {
                    "targets": [0,3,4], //first column / numbering column
                    "orderable": false
                }
            ],
            //pageLength: 10,
            //responsive: true,
            /*'columnDefs': [
             {
             'targets': 0,
             'checkboxes': {
             'selectRow': true
             }
             }
             ],*/
            'select': {
                'style': 'multi'
            },
            'deferRender': true,
            "scrollX": true
            /*columns: [
             {
             data: 'id',
             name: 'id',
             render: function (data, type, full, meta) {
             return '<input type="checkbox" class="child" name="id[]" value="' + data + '">';
             },
             sortable: false,
             searchable: false
             },
             ]*/
            ///"fnDrawCallback": function (oSettings) {},
            /*"iDisplayLength": 10,
             "aLengthMenu": [
             [5, 10, 15, -1],
             [5, 10, 15, "All"]
             ],*/
            //"scrollX": true
        });
    };



    $scope.filterStatusWiseFun = function() {
        console.log($scope.reqData.filterStatus);
        //$scope.getListFun();
        $scope.listTable.ajax.reload(null, false); //reload datatable ajax
        //$scope.getListFun();
    }
    $scope.bulkActionFun = function() {
        var rows_selected = $scope.listTable.column(0).checkboxes.selected();
        console.log(rows_selected.join(","));
        $.each(rows_selected, function(index, rowId){
            // Create a hidden element
            //console.log(rowId);
            /*$(form).append(
             $('<input>')
             .attr('type', 'hidden')
             .attr('name', 'id[]')
             .val(rowId)
             );*/
        });
        console.log(rows_selected );
        //$scope.listTable.ajax.reload(null, false); //reload datatable ajax
        //$scope.getListFun();
    }

    /*$scope.toggleCheckFun = () => {
     $scope.checkAll = !$scope.checkAll;
     if ($scope.checkAll) {
     //$scope.isShowAssignBtn = true;
     $scope.reqData.aList = $scope.reqData.aList.map(function (productRow) { return productRow.productId; });
     } else {
     $scope.assignReqData.productIdArr = [];
     }
     }*/


}
]);


app.controller('listingCtrlWITHCSRF', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnBuilder','DTColumnDefBuilder',
    function ($scope, $http, DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder) {
        //var canceler = $q.defer();
        //$scope.pager = {};
        //$scope.reqData = { isLoading: true, perPage: 10, page: 1, search: '' };
        $scope.requestData = {};
        //$scope.listData = [];
        $scope.dtInstance = {};
        $scope.instances = [];
        $scope.dtColumns = [
            //here We will add .withOption('name','column_name') for send column name to the server
            DTColumnBuilder.newColumn("id", "Sr.#").withOption('name', 'id'),
            DTColumnBuilder.newColumn("name", "Name").withOption('name', 'name'),
            DTColumnBuilder.newColumn("price", "Price").withOption('name', 'price'),
            DTColumnBuilder.newColumn("status", "Status").withOption('name', 'status'),
            DTColumnBuilder.newColumn("action", "Action").withOption('name', 'action'),
        ];
        $scope.dtColumnDefs = [
            DTColumnDefBuilder.newColumnDef(0).notSortable(),
            DTColumnDefBuilder.newColumnDef(3).notSortable(),
            DTColumnDefBuilder.newColumnDef(4).notSortable()
        ];
        $scope.fetchList =()  => {
            //$scope.reqData.isLoading = true;
            //canceler.resolve();
            //canceler = $q.defer();
            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                /*dataSrc: "data",
                 url: urlList,
                 type:"POST"*/

                type: 'POST',
                url: urlList,
                //contentType: 'application/json',
                //processData: false,
                //beforeSend: function(xhr, settings) {
                //...
                //},
                //data: function(data) {
                //...
                //}
                headers: {
                    'X-CSRF-TOKEN': CSRFTOKEN
                },
                data: function(d) {
                    d.limit = d.length;
                },
                dataSrc: function(json) {
                    //console.log('SUCCESS')
                    //console.log(json.data)
                    //$scope.listData = json.data;
                    return json.data
                },
                //complete still works
                complete: function(jqXHR, textStatus) {
                    //  console.log(jqXHR.responseText)
                }
            })
                .withOption('processing', true) //for show progress bar
                .withOption('serverSide', true) // for server side processing
                .withPaginationType('full_numbers') // for get full pagination options // first / last / prev / next and page numbers
                ///.withDisplayLength(10) // Page size
                //.withOption('aaSorting',[1,'asc']) // for default sorting column // here 0 means first column
                .withOption('order',[]) // for default sorting column // here 0 means first column
        }

        $scope.fetchList();
        $scope.reloadData = function() {
            $scope.requestData.searchByStatus = 'Active';
            $scope.dtInstance.rerender();
        }

    }
]);
