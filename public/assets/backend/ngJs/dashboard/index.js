var moduleNameURL = '/demo-crud/';
var moduleName = 'demo-crud/';
app.config(function($locationProvider,$routeProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider
        .when(moduleNameURL +"list", {
            templateUrl: 'listing.html',
            controller: 'listingCtrl'
        })
        .when(moduleNameURL +"add", {
            templateUrl: 'add.html',
            controller: 'addCtrl'
        })
        .when(moduleNameURL +"update/:id", {
            templateUrl: 'update.html',
            controller: 'addCtrl'
        })
        .when("/green", {
            templateUrl : "green.htm"
        })
        .when("/blue", {
            templateUrl : "blue.htm"
        })
        .otherwise({
          //  redirectTo: moduleNameURL +'list'
        });
});
