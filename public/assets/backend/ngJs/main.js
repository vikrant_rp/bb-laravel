var loader = '<div class="Box">Loading...<span></span></div>';
var loader_inner = '<span class="loader-inner"></span></h3>';
var msgError = 'Something went wrong...!!!';
var msgErrorProductQtyZero = "you have reached the minimum quantity for a product.";
var msgErrorMinimumProductLimit = 'Please select minimum products';
var msgStripeTransactionProcessing = "Note: Please do not press 'Refresh' or 'Back' button";
var base_url ='';
//var app = angular.module('app', ['ui.router','ui.utils','datatables']);
//var app = angular.module('app', ['ui.router']);
//var app = angular.module('app', ['ngRoute']);
var app = angular.module("myApp", ["ngRoute","ngValidate","cp.ngConfirm"]);
//https://www.tutsmake.com/laravel-6-angular-js-crud-example-tutorial/
//var app = angular.module('customerRecords', []).constant('API_URL', 'http://localhost:8000/api/v1/');
//app.constant('BASE_URL', 'http://localhost:8000/api/v1/');
/*var base_url = window.location.origin;
var host = window.location.host;
var pathArray = window.location.pathname.split( '/' );

console.log(location);
console.log(base_url);
console.log(host);
console.log(pathArray);*/
app.constant('config', {
    baseUrl: window.location.origin+'/',
    apiUrl: baseURLJS,
    enableDebug: true
});


function goToUpFun() {
    jQuery(window).scrollTop(0);
}
