app.run(function($rootScope) {
    $rootScope.dropdown=function(id){
        $('#'+id).multiselect({
            templates: { // Use the Awesome Bootstrap Checkbox structure
                li: '<li><div class="checkbox"><label></label></div></li>'
            },
            nonSelectedText:'Select Type',
            numberDisplayed: 1,
        });
        $('.multiselect-container div.checkbox').each(function (index) {

            var id = 'multiselect-' + index,
                $input = $(this).find('input');

            // Associate the label and the input
            $(this).find('label').attr('for', id);
            $input.attr('id', id);

            // Remove the input from the label wrapper
            $input.detach();

            // Place the input back in before the label
            $input.prependTo($(this));

            $(this).click(function (e) {
                // Prevents the click from bubbling up and hiding the dropdown
                e.stopPropagation();
            });

        });

    };
    $rootScope.export=function(chart,dataFields,href,csvhref,name){
        chart.exporting.menu = new am4core.ExportMenu();
        if(dataFields != "") {
            chart.exporting.dataFields = dataFields;
        }
        chart.exporting.filePrefix = name;
        if(csvhref!="") {
            chart.exporting.menu.items = [{
                "label": '<a  class="btn btn-primary float-right btn-top-download ml-3" title="Export"><i class="fa fa-download"></i></a>',
                "menu": [
                    {
                        "type": "custom", "label": "PDF", "options": {
                            callback: function () {
                                window.open(href, '_blank');
                            }
                        }
                    },
                    {
                        "type": "custom", "label": "CSV", "options": {
                            callback: function () {
                                window.open(csvhref, '_blank');
                            }
                        }
                    },
                    /* { "type": "csv", "label": "XLSX" },
               { "type": "xlsx", "label": "XLSX" }*/
                ]
            }];
        }else
            {
                chart.exporting.menu.items = [{
                    "label": '<a  class="btn btn-primary float-right btn-top-download ml-3" title="Export"><i class="fa fa-download"></i></a>',
                    "menu": [
                        {
                            "type": "custom", "label": "PDF", "options": {
                                callback: function () {
                                    window.open(href, '_blank');
                                }
                            }
                        },
                       { "type": "csv", "label": "CSV" },
                        /*  { "type": "xlsx", "label": "XLSX" }*/
                    ]
                }];
            }
    }
    //////////////////////////// Revenue Stream chart display //////////////////////////////////////
    $rootScope.revenueStreamchart=function(data)
    {
        console.log(data);
        if ( data=="")
        {
            $('#amountChart').html('<div class="col-sm-12 mt-3"><div class="col-sm-12 alert alert-danger pichartalert">No Data Available</div></div>');
        }else {
            am4core.useTheme(am4themes_animated);

            var chart = am4core.create("amountChart", am4charts.PieChart);
            chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

            chart.data = data;
            chart.radius = am4core.percent(70);
            chart.innerRadius = am4core.percent(40);
            chart.startAngle = 180;
            chart.endAngle = 360;
         //   chart.visible = false;

            var series = chart.series.push(new am4charts.PieSeries());
            series.dataFields.value = "value";
            series.dataFields.category = "country";
            series.slices.template.propertyFields.dummyData = "breakdown";
            series.slices.template.tooltipText = "[bold]{category}[/] ({value.percent.formatNumber('#.0')}%)\nCount: #{dummyData.totalcount}\nAmount: ₦{dummyData.totalamount}";
            series.labels.template.disabled = true;
            series.ticks.template.disabled = true;
            series.slices.template.cornerRadius = 0;
            series.slices.template.innerCornerRadius = 0;
            series.slices.template.draggable = true;
            series.slices.template.inert = true;
            series.alignLabels = false;

            series.hiddenState.properties.startAngle = 90;
            series.hiddenState.properties.endAngle = 90;
            series.colors.list = [
                am4core.color("#2B8969"),
                am4core.color("#333333"),
                am4core.color("#707C7C"),
                am4core.color("#30649B")
            ];
            chart.legend = new am4charts.Legend();
            /* var dataFields= {country:"Revenue Stream",
                 value:"Amount"};
             var href="lgDashboardChart/pdfExport";
             var csvhref="lgDashboardChart/sectorWiseCsvexport";
             $rootScope.export(chart,dataFields,href,csvhref,"collectionAmountSectorWise");*/
        }
    };
    $rootScope.gauageChart=function(percentageValue){
        //////////////////////////// Consumed and Unconsumed  Chart //////////////////////////////////////
        am4core.useTheme(am4themes_dataviz);
        am4core.useTheme(am4themes_animated);
        var chart = am4core.create("consumedChart", am4charts.GaugeChart);
        chart.innerRadius = am4core.percent(82);

        /**
         * Normal axis
         */

        var axis = chart.xAxes.push(new am4charts.ValueAxis());
        axis.min = 0;
        axis.max = 100;
        axis.strictMinMax = true;
        axis.renderer.radius = am4core.percent(80);
        axis.renderer.inside = true;
        axis.renderer.line.strokeOpacity = 1;
        axis.renderer.ticks.template.strokeOpacity = 1;
        axis.renderer.ticks.template.length = 10;
        axis.renderer.grid.template.disabled = true;
        axis.renderer.labels.template.radius = 25;
        axis.renderer.labels.template.fontSize = 10;
        axis.renderer.labels.template.adapter.add("text", function(text) {
            return text + "%";
        })

        /**
         * Axis for ranges
         */

        var colorSet = new am4core.ColorSet();

        var axis2 = chart.xAxes.push(new am4charts.ValueAxis());
        axis2.min = 0;
        axis2.max = 100;
        axis2.renderer.innerRadius = 10;
        axis2.strictMinMax = true;
        axis2.renderer.labels.template.disabled = true;
        axis2.renderer.ticks.template.disabled = true;
        axis2.renderer.grid.template.disabled = true;

        var range0 = axis2.axisRanges.create();
        range0.value = 0;
        range0.endValue = 50;
        range0.axisFill.fillOpacity = 1;
        range0.axisFill.fill = colorSet.getIndex(0);
        range0.axisFill.fill =  am4core.color("#30649B");

        var range1 = axis2.axisRanges.create();
        range1.value = 50;
        range1.endValue = 100;
        range1.axisFill.fillOpacity = 1;
        range1.axisFill.fill =  am4core.color("#2B8969");
        /**
         * Label
         */

        var label = chart.radarContainer.createChild(am4core.Label);
        label.isMeasured = false;
        label.fontSize = 16;
		label.fontWeight = 900;
		label.fill =  am4core.color("#30649B");
        label.x = am4core.percent(50);
        label.y = am4core.percent(100);
        label.horizontalCenter = "top";
        label.verticalCenter = "top";
        label.text = percentageValue+'%';


        /**
         * Hand
         */

        var hand = chart.hands.push(new am4charts.ClockHand());
        hand.axis = axis2;
        hand.innerRadius = am4core.percent(10);
        hand.startWidth = 10;
        hand.pin.disabled = true;
        hand.value =percentageValue;

        hand.events.on("propertychanged", function(ev) {
            range0.endValue = ev.target.value;
            range1.value = ev.target.value;
            axis2.invalidate();
        });
       // chart.exporting.menu = new am4core.ExportMenu();
        /* setInterval(() => {
             var value = Math.round(Math.random() * 100);
             label.text = value + "%";
             var animation = new am4core.Animation(hand, {
                 property: "value",
                 to: value
             }, 1000, am4core.ease.cubicOut).start();
         }, 2000);*/

    }
});
