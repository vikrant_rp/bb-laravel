//https://craftpip.github.io/angular-confirm/
app.factory('bbNgNotify', ['$window','$ngConfirm', function(msg,$ngConfirm) {
    var jsNotify = {};
    //https://craftpip.github.io/angular-confirm/app.js
    jsNotify.notifyDefaultAutoClose = function(msg,title) {
        //$ngConfirm('Deleted the user!',2000);
        $ngConfirm({
            title: title,// 'Congratulations!',
            type: 'dark',
            content: msg,//'Consider something great happened, and you have to show a positive message',
            autoClose: 'close|3000',
            buttons: {
                close: function () {
                }
            }
        })
    };
    jsNotify.notifySuccessAutoClose = function(msg,title='Success') {
        //$ngConfirm('Deleted the user!',2000);
        $ngConfirm({
            title: title,
            type: 'green',
            content: msg,//'Consider something great happened, and you have to show a positive message',
            autoClose: 'close|3000',
            //icon: 'fa fa-warning',
           // title: 'font-awesome'
            buttons: {
                close: function () {
                }
            }
        })
    };
    jsNotify.notifyErrorAutoClose = function(msg,title) {
        //$ngConfirm('Deleted the user!',2000);
        $ngConfirm({
            //animation: 'none',
            //closeAnimation:'none',
            title: '<span class="required text-danger">'+title+'</span>',
            type: 'red',
            content: '<span class="required text-danger">'+msg+'</span>',//'Consider something great happened, and you have to show a positive message',
            autoClose: 'close|5000',
            //icon: 'fa fa-warning',
            // title: 'font-awesome'
            buttons: {
                close: function () {
                }
            }
        })
    };
    return jsNotify;
}]);