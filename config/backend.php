<?php

return [
    'colors' => [
        'default' => 'default',
        'primary' => 'primary',
        'secondary' => 'secondary',
        'success' => 'success',
        'danger' => 'danger',
        'warning' => 'warning',
    ],
    /*'status' => [
        '0' => 'In-Active',
        '1' => 'Active'
    ],*/
    'status' => [ //Table Name and their status
        'tests' => [
            0 => ['label'=>'Delete','color'=>''],
            1 => ['label'=>'Active','color'=>'success'],
            2 => ['label'=>'In-Active','color'=>'danger'],
        ]
    ]
];
