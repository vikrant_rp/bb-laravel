<?php

return [
    'backend_assets_url' => env('APP_URL', 'http://localhost').'assets/backend/'.env('THEME_NAME'),
    'front_assets_url' => env('APP_URL', 'http://localhost').'assets/backend/'.env('THEME_NAME'),
    'mails' => [
        'site_name' => env('APP_NAME', 'Laravel'),
        'site_info_email' => 'info@slatesign.co.uk',
        #'dev_emails' => ['vikas.maheshwari1991.vm@gmail.com', 'parmarvikrantr@gmail.com','hello@gecko.media'],
        'dev_emails' => ['parmarvikrantr@gmail.com'],
        'bcc_emails' => ['parmarvikrantr@gmail.com'],
        'sales_emails' => ['parmarvikrantr@gmail.com'],
        'dev_team_email' => [
            'place_order_error'=> 'Error Place Order',
            'place_order_before'=> 'Before: Place Order Request Data',
            'cron_gift_voucher_send'=> "Daily Gift Voucher Reminder | House Sign",
        ],
        'cron_email'=>[
            'cron_gift_voucher_send' => "Daily Gift Voucher Reminder | House Sign",
        ],
        'front_email' =>[
            'place_order_success' => 'Order | House Sign',
            'gift_voucher_send' => 'Voucher Code | House Sign',
            'contact_form' => 'Contact Form Enquiry',
            'test_email' => 'Test Email From House Sign',
        ],
        'admin_email' =>[
            'admin_place_order_success' => 'New Order Placed Successfully',
            'admin_change_status_mail' => 'Your Order Status Change',
        ]
    ],


];
